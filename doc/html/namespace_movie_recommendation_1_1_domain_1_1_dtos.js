var namespace_movie_recommendation_1_1_domain_1_1_dtos =
[
    [ "CommentAndVoteDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_comment_and_vote_dto.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_comment_and_vote_dto" ],
    [ "ConfMailServer", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_conf_mail_server.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_conf_mail_server" ],
    [ "CronJobStatusModel", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_cron_job_status_model.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_cron_job_status_model" ],
    [ "JwtConfigModel", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_jwt_config_model.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_jwt_config_model" ],
    [ "LoginDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_login_dto.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_login_dto" ],
    [ "OperationClaim", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_operation_claim.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_operation_claim" ],
    [ "PaginationDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_pagination_dto.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_pagination_dto" ],
    [ "ResultModel", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_model.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_model" ],
    [ "ResultMovieDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto" ],
    [ "ResultMovieWithPaginationModel", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_with_pagination_model.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_with_pagination_model" ],
    [ "TmdbApiInfo", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_api_info.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_api_info" ],
    [ "TmdbRequestDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_request_dto.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_request_dto" ],
    [ "TmDbResult", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result" ],
    [ "UserResponseDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_user_response_dto.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_user_response_dto" ]
];