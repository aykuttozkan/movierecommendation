var interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_conf_mail_server =
[
    [ "CredentialPass", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_conf_mail_server.html#aa5ae12359c318736121c38a8e509600e", null ],
    [ "CredentialUser", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_conf_mail_server.html#a78f433332d2d452415ab464abd7ffcf7", null ],
    [ "EnableSSL", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_conf_mail_server.html#ab11efc2aa185d7f91d112eee35b0b3d7", null ],
    [ "Host", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_conf_mail_server.html#a184ad3b7d04545b556148e7aca3d3251", null ],
    [ "SmtpPort", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_conf_mail_server.html#a3c1ce8a1fe0a53bc23da4cbf5fe19821", null ]
];