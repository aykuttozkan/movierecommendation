var dir_3e3301ad0f960202d9226560631ef2c1 =
[
    [ "CommentAndVoteDto.cs", "_comment_and_vote_dto_8cs.html", [
      [ "CommentAndVoteDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_comment_and_vote_dto.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_comment_and_vote_dto" ]
    ] ],
    [ "ConfMailServer.cs", "_conf_mail_server_8cs.html", [
      [ "ConfMailServer", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_conf_mail_server.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_conf_mail_server" ]
    ] ],
    [ "CronJobStatusModel.cs", "_cron_job_status_model_8cs.html", [
      [ "CronJobStatusModel", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_cron_job_status_model.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_cron_job_status_model" ]
    ] ],
    [ "JwtConfigModel.cs", "_jwt_config_model_8cs.html", [
      [ "JwtConfigModel", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_jwt_config_model.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_jwt_config_model" ]
    ] ],
    [ "LoginDto.cs", "_login_dto_8cs.html", [
      [ "LoginDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_login_dto.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_login_dto" ]
    ] ],
    [ "OperationClaim.cs", "_operation_claim_8cs.html", [
      [ "OperationClaim", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_operation_claim.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_operation_claim" ]
    ] ],
    [ "PaginationDto.cs", "_pagination_dto_8cs.html", [
      [ "PaginationDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_pagination_dto.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_pagination_dto" ]
    ] ],
    [ "ResultModel.cs", "_result_model_8cs.html", [
      [ "ResultModel", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_model.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_model" ]
    ] ],
    [ "ResultMovieDto.cs", "_result_movie_dto_8cs.html", [
      [ "ResultMovieDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto" ]
    ] ],
    [ "ResultMovieWithPaginationModel.cs", "_result_movie_with_pagination_model_8cs.html", [
      [ "ResultMovieWithPaginationModel", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_with_pagination_model.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_with_pagination_model" ]
    ] ],
    [ "TmdbApiInfo.cs", "_tmdb_api_info_8cs.html", [
      [ "TmdbApiInfo", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_api_info.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_api_info" ]
    ] ],
    [ "TmdbRequestDto.cs", "_tmdb_request_dto_8cs.html", [
      [ "TmdbRequestDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_request_dto.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_request_dto" ],
      [ "TmDbResult", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result" ]
    ] ],
    [ "UserResponseDto.cs", "_user_response_dto_8cs.html", [
      [ "UserResponseDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_user_response_dto.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_user_response_dto" ]
    ] ]
];