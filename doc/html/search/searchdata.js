var indexSectionsWithContent =
{
  0: ".2_abcdefghijlmnoprstuvw",
  1: "_abcdehijlmoprstuvw",
  2: "m",
  3: ".2abcehijlmoprstuvw",
  4: "abcdfghilmoprstuvw",
  5: "_cilmorstuv",
  6: "_abcdeghimnoprstuv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Properties"
};

