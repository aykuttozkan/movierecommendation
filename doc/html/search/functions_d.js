var searchData=
[
  ['sendemailasync_571',['SendEmailAsync',['../class_movie_recommendation_1_1_core_1_1_tools_1_1_mail_tools.html#ad694cad941e14e32b2e25e7550fcbb51',1,'MovieRecommendation.Core.Tools.MailTools.SendEmailAsync()'],['../interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_mail_tools.html#a0f8b293b68ab8d6d3f4c7e0aef9bcf08',1,'MovieRecommendation.Domain.Abstract.IMailTools.SendEmailAsync()']]],
  ['sendmail_572',['SendMail',['../class_movie_recommendation_1_1_x_unit_test_1_1_mail_tools_test.html#aa028996c504c516613cf8246935a76e4',1,'MovieRecommendation::XUnitTest::MailToolsTest']]],
  ['sendtomail_573',['SendToMail',['../class_movie_recommendation_1_1_bll_1_1_concrete_1_1_mail_service.html#aa90610ce694d090e27cc18a9b650756f',1,'MovieRecommendation.Bll.Concrete.MailService.SendToMail()'],['../interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_mail_service.html#a774e37ae8977bb18f5d80746117d6fea',1,'MovieRecommendation.Domain.Interfaces.IMailService.SendToMail()']]],
  ['setclaims_574',['SetClaims',['../class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_account_controller.html#ab0774693d1888a76b776701872edac09',1,'MovieRecommendation::WebApi::Controllers::AccountController']]],
  ['startup_575',['Startup',['../class_movie_recommendation_1_1_web_api_1_1_startup.html#ac3ddf1ae9e244e7a0d66aa4ff3db4636',1,'MovieRecommendation::WebApi::Startup']]],
  ['stringextentionstest_576',['StringExtentionsTest',['../class_movie_recommendation_1_1_x_unit_test_1_1_string_extentions_test.html#a35a90dec4486efedfc74aa5c81418f54',1,'MovieRecommendation::XUnitTest::StringExtentionsTest']]]
];
