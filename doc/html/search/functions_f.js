var searchData=
[
  ['unitofwork_582',['UnitOfWork',['../class_movie_recommendation_1_1_dal_1_1_unit_of_work.html#aef76e0e51160be4c7cc9deca4c68ddcd',1,'MovieRecommendation::Dal::UnitOfWork']]],
  ['up_583',['Up',['../class_movie_recommendation_1_1_dal_1_1_migrations_1_1__01___init.html#a6e06c30138c2cd84695d2eda2b851675',1,'MovieRecommendation.Dal.Migrations._01_Init.Up()'],['../class_movie_recommendation_1_1_dal_1_1_migrations_1_1__02__add_default_user.html#a6f1c0bcaeea10a79f3aae593a746e049',1,'MovieRecommendation.Dal.Migrations._02_addDefaultUser.Up()']]],
  ['updatemovie_584',['UpdateMovie',['../class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html#a78ef4c8194240d4c0d7dfa5431a5e3f2',1,'MovieRecommendation.Bll.Concrete.MovieManager.UpdateMovie()'],['../interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_movie_service.html#ac9a1c3c24f30ba3b4ba97c6f0d98ad2a',1,'MovieRecommendation.Domain.Interfaces.IMovieService.UpdateMovie()']]],
  ['usermanager_585',['UserManager',['../class_movie_recommendation_1_1_bll_1_1_concrete_1_1_user_manager.html#a990a4e7cf8c83821922a878536dc4c4f',1,'MovieRecommendation::Bll::Concrete::UserManager']]],
  ['usermanagertest_586',['UserManagerTest',['../class_movie_recommendation_1_1_x_unit_test_1_1_user_manager_test.html#a0232734ed3642d3cf0e1faa5cfe9fecf',1,'MovieRecommendation::XUnitTest::UserManagerTest']]],
  ['userrepository_587',['UserRepository',['../class_movie_recommendation_1_1_dal_1_1_concrete_1_1_user_repository.html#a978c2b05b5b6a907996b819016a09288',1,'MovieRecommendation::Dal::Concrete::UserRepository']]]
];
