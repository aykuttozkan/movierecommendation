var searchData=
[
  ['commitasync_510',['CommitAsync',['../class_movie_recommendation_1_1_dal_1_1_unit_of_work.html#a72a762a8fedd7811711dcfc2c720bae7',1,'MovieRecommendation.Dal.UnitOfWork.CommitAsync()'],['../interface_movie_recommendation_1_1_domain_1_1_i_unit_of_work.html#a2c6730f78f390f04e60a29e692441b09',1,'MovieRecommendation.Domain.IUnitOfWork.CommitAsync()']]],
  ['configure_511',['Configure',['../class_movie_recommendation_1_1_web_api_1_1_startup.html#ad04cbebe92fbe3749c60806fc0a4e61b',1,'MovieRecommendation::WebApi::Startup']]],
  ['configureservices_512',['ConfigureServices',['../class_movie_recommendation_1_1_web_api_1_1_startup.html#a891af4f13211d851c4b98cb9afdc1d60',1,'MovieRecommendation::WebApi::Startup']]],
  ['createhostbuilder_513',['CreateHostBuilder',['../class_movie_recommendation_1_1_web_api_1_1_program.html#a1dfdd724b79bfdb8fcb48a60c6342ebe',1,'MovieRecommendation::WebApi::Program']]],
  ['createmovie_514',['CreateMovie',['../class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html#a90d4ccc5ad77be2f42fd47c23ba6b78e',1,'MovieRecommendation.Bll.Concrete.MovieManager.CreateMovie()'],['../interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_movie_service.html#a6eea43462232b7afae9f0f21f36159f4',1,'MovieRecommendation.Domain.Interfaces.IMovieService.CreateMovie()']]],
  ['cronjobmanager_515',['CronJobManager',['../class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager.html#af391143bd8d72a0ca006e8f87e64cd43',1,'MovieRecommendation::Bll::Concrete::CronJobManager']]],
  ['cronmovieapi_516',['CronMovieApi',['../class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_cron_movie_api.html#a65fcc2874c5c3aff1f9075872bc89928',1,'MovieRecommendation::WebApi::Controllers::CronMovieApi']]]
];
