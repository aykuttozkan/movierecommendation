var searchData=
[
  ['delay_517',['Delay',['../class_movie_recommendation_1_1_core_1_1_tools_1_1_thread_tools.html#a8c5b1cf38ee7687e0c3652470533fa9a',1,'MovieRecommendation::Core::Tools::ThreadTools']]],
  ['deletemovie_518',['DeleteMovie',['../class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html#a90a0a133293edd94d37787038cee42ec',1,'MovieRecommendation.Bll.Concrete.MovieManager.DeleteMovie()'],['../interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_movie_service.html#a9b4f4285ffc8b47f67570775e952c44a',1,'MovieRecommendation.Domain.Interfaces.IMovieService.DeleteMovie()']]],
  ['dependencysetupfixture_519',['DependencySetupFixture',['../class_movie_recommendation_1_1_x_unit_test_1_1_dependency_setup_fixture.html#a55e7e1c3b8dbddde45bd95738c1d3458',1,'MovieRecommendation::XUnitTest::DependencySetupFixture']]],
  ['dispose_520',['Dispose',['../class_movie_recommendation_1_1_dal_1_1_unit_of_work.html#a2f62edcddc922aef3c7e5e7a16a1a054',1,'MovieRecommendation::Dal::UnitOfWork']]],
  ['down_521',['Down',['../class_movie_recommendation_1_1_dal_1_1_migrations_1_1__01___init.html#a43773d585e699b6b3e6a0c81c64f82f7',1,'MovieRecommendation.Dal.Migrations._01_Init.Down()'],['../class_movie_recommendation_1_1_dal_1_1_migrations_1_1__02__add_default_user.html#a37c5a50bc8a9ddfb626e49239a9496e4',1,'MovieRecommendation.Dal.Migrations._02_addDefaultUser.Down()']]]
];
