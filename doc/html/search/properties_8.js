var searchData=
[
  ['id_638',['Id',['../class_movie_recommendation_1_1_domain_1_1_abstract_1_1_base_entity.html#a6fccb0fea579b649c9afe824cf6ae25e',1,'MovieRecommendation.Domain.Abstract.BaseEntity.Id()'],['../class_movie_recommendation_1_1_domain_1_1_dtos_1_1_operation_claim.html#a5255a5174fd943006c2c0798e8b67baf',1,'MovieRecommendation.Domain.Dtos.OperationClaim.Id()'],['../class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html#af1637bf282211acfed162333df017555',1,'MovieRecommendation.Domain.Dtos.ResultMovieDto.Id()']]],
  ['id_639',['id',['../class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html#a63e448c5e6134438fa0554d0b594e465',1,'MovieRecommendation::Domain::Dtos::TmDbResult']]],
  ['imageurl_640',['ImageUrl',['../interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_tmdb_api_info.html#afa8f90a064beeaa2492039c218ffd9a6',1,'MovieRecommendation.Domain.Abstract.ITmdbApiInfo.ImageUrl()'],['../class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_api_info.html#ade2d704558a4454e9005d551d1f71d91',1,'MovieRecommendation.Domain.Dtos.TmdbApiInfo.ImageUrl()']]],
  ['isactive_641',['IsActive',['../class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html#a25585f510cea4160e01d6604876c5f9d',1,'MovieRecommendation::Domain::Entities::Movie']]],
  ['isdelete_642',['IsDelete',['../class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html#a15e453555d72f97ecba6f8490a261887',1,'MovieRecommendation::Domain::Entities::Movie']]],
  ['issuer_643',['Issuer',['../class_movie_recommendation_1_1_domain_1_1_dtos_1_1_jwt_config_model.html#a7971ae04c98d77f0b78be86754f0f80b',1,'MovieRecommendation::Domain::Dtos::JwtConfigModel']]],
  ['itemperpage_644',['ItemPerPage',['../class_movie_recommendation_1_1_domain_1_1_dtos_1_1_pagination_dto.html#aab9286ebaef405f3d3191de2b1f8ec88',1,'MovieRecommendation::Domain::Dtos::PaginationDto']]]
];
