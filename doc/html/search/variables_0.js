var searchData=
[
  ['_5fconfmailserver_591',['_ConfMailServer',['../class_movie_recommendation_1_1_core_1_1_tools_1_1_mail_tools.html#a3e0e0210d5693175ebdf058a9304520e',1,'MovieRecommendation::Core::Tools::MailTools']]],
  ['_5fcontext_592',['_Context',['../class_movie_recommendation_1_1_dal_1_1_unit_of_work.html#a9b97e4bfaf7f9911e912d66ca456ceba',1,'MovieRecommendation::Dal::UnitOfWork']]],
  ['_5fjwtconfigmodel_593',['_JwtConfigModel',['../class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_account_controller.html#ae913b8e14bbfc0772d3009405528e152',1,'MovieRecommendation::WebApi::Controllers::AccountController']]],
  ['_5flogger_594',['_logger',['../class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_weather_forecast_controller.html#af589fa6aa6cfe28e4d3cf0d205ef1dcc',1,'MovieRecommendation::WebApi::Controllers::WeatherForecastController']]],
  ['_5fmailtools_595',['_MailTools',['../class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html#aba034a2832b77ad48fc3f9546eaa4d66',1,'MovieRecommendation::Bll::Concrete::MovieManager']]],
  ['_5fmovierepository_596',['_MovieRepository',['../class_movie_recommendation_1_1_dal_1_1_unit_of_work.html#abb9bf57ebdb3a35df931556fa43a91a1',1,'MovieRecommendation::Dal::UnitOfWork']]],
  ['_5forderby_597',['_OrderBy',['../class_movie_recommendation_1_1_domain_1_1_dtos_1_1_pagination_dto.html#abb77f55820a42655665dbfeb405144d0',1,'MovieRecommendation::Domain::Dtos::PaginationDto']]],
  ['_5fserviceprovide_598',['_serviceProvide',['../class_movie_recommendation_1_1_x_unit_test_1_1_mail_tools_test.html#a4c7224797fb76c1068da0ed1ded09a53',1,'MovieRecommendation::XUnitTest::MailToolsTest']]],
  ['_5ftmdbapiinfo_599',['_TmdbApiInfo',['../class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html#a27067d87938485c0a0876e31cd614d52',1,'MovieRecommendation::Bll::Concrete::MovieManager']]],
  ['_5funitofwork_600',['_UnitOfWork',['../class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html#a2269e94d55a9ba95a96f6e16f608e2e3',1,'MovieRecommendation.Bll.Concrete.MovieManager._UnitOfWork()'],['../class_movie_recommendation_1_1_bll_1_1_concrete_1_1_user_manager.html#a5933db8344943e88757d51045cb7988f',1,'MovieRecommendation.Bll.Concrete.UserManager._UnitOfWork()'],['../class_movie_recommendation_1_1_bll_1_1_concrete_1_1_vote_manager.html#ae283ec6b8bf0304f5f58217ec0e831d2',1,'MovieRecommendation.Bll.Concrete.VoteManager._UnitOfWork()']]],
  ['_5fuserrepository_601',['_UserRepository',['../class_movie_recommendation_1_1_dal_1_1_unit_of_work.html#afc63e9854a28fe5dfbcf268134e88875',1,'MovieRecommendation::Dal::UnitOfWork']]],
  ['_5fvoterepository_602',['_VoteRepository',['../class_movie_recommendation_1_1_dal_1_1_unit_of_work.html#aad8db430e05ff42024c72f5dd7e75f5d',1,'MovieRecommendation::Dal::UnitOfWork']]]
];
