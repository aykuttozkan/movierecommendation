var searchData=
[
  ['backdrop_5fpath_31',['backdrop_path',['../class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html#acac3b2797ff9b775939c6825c2b3cd29',1,'MovieRecommendation::Domain::Dtos::TmDbResult']]],
  ['backdroppath_32',['BackdropPath',['../class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html#ac942f0258fd861c8a449f4dbff367560',1,'MovieRecommendation.Domain.Dtos.ResultMovieDto.BackdropPath()'],['../class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html#aa20f6e6da3adb8378744c1b5aee6ff4e',1,'MovieRecommendation.Domain.Entities.Movie.BackdropPath()']]],
  ['baseentity_33',['BaseEntity',['../class_movie_recommendation_1_1_domain_1_1_abstract_1_1_base_entity.html',1,'MovieRecommendation::Domain::Abstract']]],
  ['baseentity_2ecs_34',['BaseEntity.cs',['../_base_entity_8cs.html',1,'']]],
  ['baseentity_3c_20int_20_3e_35',['BaseEntity&lt; int &gt;',['../class_movie_recommendation_1_1_domain_1_1_abstract_1_1_base_entity.html',1,'MovieRecommendation::Domain::Abstract']]],
  ['buildmodel_36',['BuildModel',['../class_movie_recommendation_1_1_dal_1_1_migrations_1_1_movie_db_context_model_snapshot.html#a8f19e46aa60df1e5189806824291555a',1,'MovieRecommendation::Dal::Migrations::MovieDbContextModelSnapshot']]],
  ['buildtargetmodel_37',['BuildTargetModel',['../class_movie_recommendation_1_1_dal_1_1_migrations_1_1__01___init.html#a0b606a440b977c5fe4a718017b418080',1,'MovieRecommendation.Dal.Migrations._01_Init.BuildTargetModel()'],['../class_movie_recommendation_1_1_dal_1_1_migrations_1_1__02__add_default_user.html#a16390b50b17062bc82a569a1eacf2102',1,'MovieRecommendation.Dal.Migrations._02_addDefaultUser.BuildTargetModel()']]]
];
