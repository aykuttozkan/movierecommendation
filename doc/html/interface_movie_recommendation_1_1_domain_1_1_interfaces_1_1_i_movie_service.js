var interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_movie_service =
[
    [ "CreateMovie", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_movie_service.html#a6eea43462232b7afae9f0f21f36159f4", null ],
    [ "DeleteMovie", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_movie_service.html#a9b4f4285ffc8b47f67570775e952c44a", null ],
    [ "GetAllMovies", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_movie_service.html#a40ddfe1ba50dc9ca57c5f9e5c997c95a", null ],
    [ "GetAllMoviesByPageFilter", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_movie_service.html#aeb21dfdb92f98795055296fd0665670f", null ],
    [ "GetMovieById", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_movie_service.html#a9235c64c8cc5a6bd75d9b4cfda3fd029", null ],
    [ "GetMovieBySyncId", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_movie_service.html#a785a06fa7e8a6495dd9d164896714ef9", null ],
    [ "Recommend", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_movie_service.html#a3bd05eeaf3f23481fb777ff0caf28b0c", null ],
    [ "UpdateMovie", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_movie_service.html#ac9a1c3c24f30ba3b4ba97c6f0d98ad2a", null ]
];