var dir_9321eac68c7af312dd561db121ede1f7 =
[
    [ "Controllers", "dir_5646ef4420339630cd17840062b8b0bd.html", "dir_5646ef4420339630cd17840062b8b0bd" ],
    [ "Filters", "dir_6862623e5c202ae516bb86dacb8b79ce.html", "dir_6862623e5c202ae516bb86dacb8b79ce" ],
    [ "Hangfire", "dir_da7994c5d5c321519f57005d2243f3cc.html", "dir_da7994c5d5c321519f57005d2243f3cc" ],
    [ "obj", "dir_0963daaa422dcdd2f5cd758c58fcccf7.html", "dir_0963daaa422dcdd2f5cd758c58fcccf7" ],
    [ "Program.cs", "_program_8cs.html", [
      [ "Program", "class_movie_recommendation_1_1_web_api_1_1_program.html", "class_movie_recommendation_1_1_web_api_1_1_program" ]
    ] ],
    [ "Startup.cs", "_startup_8cs.html", [
      [ "Startup", "class_movie_recommendation_1_1_web_api_1_1_startup.html", "class_movie_recommendation_1_1_web_api_1_1_startup" ]
    ] ],
    [ "WeatherForecast.cs", "_weather_forecast_8cs.html", [
      [ "WeatherForecast", "class_movie_recommendation_1_1_web_api_1_1_weather_forecast.html", "class_movie_recommendation_1_1_web_api_1_1_weather_forecast" ]
    ] ]
];