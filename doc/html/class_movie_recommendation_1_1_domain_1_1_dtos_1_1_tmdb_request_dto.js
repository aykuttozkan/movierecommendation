var class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_request_dto =
[
    [ "page", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_request_dto.html#ac14613ec0a205b1a5b62581ccaf17f1a", null ],
    [ "results", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_request_dto.html#ad98d295f1d4983c2ad02e5dcb88ce99d", null ],
    [ "total_pages", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_request_dto.html#accb61a8508746690f6e9d8706a793ad8", null ],
    [ "total_results", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_request_dto.html#a56870899cae73d517e065dedd7ff2298", null ]
];