var dir_61b886018caf3427c524e18975b3ce5f =
[
    [ "CronJobManager.cs", "_cron_job_manager_8cs.html", [
      [ "CronJobManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager.html", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager" ]
    ] ],
    [ "MailService.cs", "_mail_service_8cs.html", [
      [ "MailService", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_mail_service.html", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_mail_service" ]
    ] ],
    [ "MovieManager.cs", "_movie_manager_8cs.html", [
      [ "MovieManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager" ]
    ] ],
    [ "UserManager.cs", "_user_manager_8cs.html", [
      [ "UserManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_user_manager.html", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_user_manager" ]
    ] ],
    [ "VoteManager.cs", "_vote_manager_8cs.html", [
      [ "VoteManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_vote_manager.html", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_vote_manager" ]
    ] ]
];