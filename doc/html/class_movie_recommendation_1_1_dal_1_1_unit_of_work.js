var class_movie_recommendation_1_1_dal_1_1_unit_of_work =
[
    [ "UnitOfWork", "class_movie_recommendation_1_1_dal_1_1_unit_of_work.html#aef76e0e51160be4c7cc9deca4c68ddcd", null ],
    [ "CommitAsync", "class_movie_recommendation_1_1_dal_1_1_unit_of_work.html#a72a762a8fedd7811711dcfc2c720bae7", null ],
    [ "Dispose", "class_movie_recommendation_1_1_dal_1_1_unit_of_work.html#a2f62edcddc922aef3c7e5e7a16a1a054", null ],
    [ "_Context", "class_movie_recommendation_1_1_dal_1_1_unit_of_work.html#a9b97e4bfaf7f9911e912d66ca456ceba", null ],
    [ "_MovieRepository", "class_movie_recommendation_1_1_dal_1_1_unit_of_work.html#abb9bf57ebdb3a35df931556fa43a91a1", null ],
    [ "_UserRepository", "class_movie_recommendation_1_1_dal_1_1_unit_of_work.html#afc63e9854a28fe5dfbcf268134e88875", null ],
    [ "_VoteRepository", "class_movie_recommendation_1_1_dal_1_1_unit_of_work.html#aad8db430e05ff42024c72f5dd7e75f5d", null ],
    [ "Transaction", "class_movie_recommendation_1_1_dal_1_1_unit_of_work.html#a633cb9241e8cf4297f069df75ded781b", null ],
    [ "Movies", "class_movie_recommendation_1_1_dal_1_1_unit_of_work.html#afcb6cee9a8f8390a69d3ef7120c8f288", null ],
    [ "Users", "class_movie_recommendation_1_1_dal_1_1_unit_of_work.html#a18c2fd387a387b14e3102633ecc25731", null ],
    [ "Votes", "class_movie_recommendation_1_1_dal_1_1_unit_of_work.html#a705bc516ed4c3b4052c508a7c699e880", null ]
];