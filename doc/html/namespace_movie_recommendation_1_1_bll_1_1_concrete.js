var namespace_movie_recommendation_1_1_bll_1_1_concrete =
[
    [ "CronJobManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager.html", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager" ],
    [ "MailService", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_mail_service.html", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_mail_service" ],
    [ "MovieManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager" ],
    [ "UserManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_user_manager.html", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_user_manager" ],
    [ "VoteManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_vote_manager.html", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_vote_manager" ]
];