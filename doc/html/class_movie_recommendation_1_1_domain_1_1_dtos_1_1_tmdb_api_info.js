var class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_api_info =
[
    [ "ApiUrl", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_api_info.html#a8c33eb6498b9d84fb73acced6c4ff2f0", null ],
    [ "Discover", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_api_info.html#aa74b94ce806e5293fb7634bd4d2e9ce2", null ],
    [ "ImageUrl", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_api_info.html#ade2d704558a4454e9005d551d1f71d91", null ],
    [ "SyncTimerTicks", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_api_info.html#aa4c96a4adf740d8d7038b44c65d03d8b", null ],
    [ "V3Auth", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_api_info.html#aea11052c55eeaa4f8ecfb48a77a48fac", null ],
    [ "V4Auth", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_api_info.html#a7fe19c669631990434b55ad53586fdee", null ]
];