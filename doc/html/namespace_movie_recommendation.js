var namespace_movie_recommendation =
[
    [ "Bll", "namespace_movie_recommendation_1_1_bll.html", "namespace_movie_recommendation_1_1_bll" ],
    [ "Core", "namespace_movie_recommendation_1_1_core.html", "namespace_movie_recommendation_1_1_core" ],
    [ "Dal", "namespace_movie_recommendation_1_1_dal.html", "namespace_movie_recommendation_1_1_dal" ],
    [ "Domain", "namespace_movie_recommendation_1_1_domain.html", "namespace_movie_recommendation_1_1_domain" ],
    [ "WebApi", "namespace_movie_recommendation_1_1_web_api.html", "namespace_movie_recommendation_1_1_web_api" ],
    [ "XUnitTest", "namespace_movie_recommendation_1_1_x_unit_test.html", "namespace_movie_recommendation_1_1_x_unit_test" ]
];