var class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager =
[
    [ "CronJobManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager.html#af391143bd8d72a0ca006e8f87e64cd43", null ],
    [ "FinishThread", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager.html#a20fe048e51e23dc9968c3dbd8681b5f9", null ],
    [ "GetDataFromTmdbWebPage", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager.html#aa2179953e2e4919c9a0657f86f55020c", null ],
    [ "GetReadDataFromTmdbWebPage", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager.html#a7ef355b94f1487a30acf5c07866ee68d", null ],
    [ "GetSyncStatusAsync", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager.html#adcf350f6f2b90b65c1b8dcf1d68ab71d", null ],
    [ "MovieCheckAndUpdateDb", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager.html#a1ad5a0ba37fc8bc514c6e583a7444e30", null ],
    [ "RunSyncTmdbMovieAsync", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager.html#a9a866aeec0c8b4fe3b3aa73f41516e12", null ],
    [ "Configuration", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager.html#a19eccb51f29bc363dc83cf6695247c2e", null ],
    [ "CounterActiveSync", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager.html#a1f0696f0dff7b5fc82e4d74f2bfa5f8a", null ],
    [ "IsRunning", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager.html#a263b56abf5d16e305a87b02cbb3899e1", null ],
    [ "ResultModelInfo", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager.html#a950a8fe951af34b2db25c9766768eec7", null ],
    [ "ServiceProvider", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager.html#a5e26bba1a71f3e69d72f94f85837fe80", null ],
    [ "TmdbApiInfo", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager.html#a1c4038f99acfd749c5c1c2fff0877cea", null ]
];