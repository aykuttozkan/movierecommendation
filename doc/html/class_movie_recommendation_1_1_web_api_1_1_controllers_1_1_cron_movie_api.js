var class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_cron_movie_api =
[
    [ "CronMovieApi", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_cron_movie_api.html#a65fcc2874c5c3aff1f9075872bc89928", null ],
    [ "GetRunSync", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_cron_movie_api.html#a78e53cb14cc138e784fe89962f1911e0", null ],
    [ "GetSyncStatus", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_cron_movie_api.html#a0576f322d83c8b408d9c3e71de1322e6", null ],
    [ "Configuration", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_cron_movie_api.html#ab920b343442b2327460b450661b7e1e2", null ],
    [ "CronJobs", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_cron_movie_api.html#a474fe6d63fd98723871d07bfbee25c49", null ],
    [ "TmdbApiInfo", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_cron_movie_api.html#a15aa19f0903bdb5abd9ec75a915f5d70", null ]
];