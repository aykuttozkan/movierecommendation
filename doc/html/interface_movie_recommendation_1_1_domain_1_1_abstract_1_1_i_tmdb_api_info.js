var interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_tmdb_api_info =
[
    [ "ApiUrl", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_tmdb_api_info.html#a8f173d5c19d69ce66095e72594c07159", null ],
    [ "Discover", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_tmdb_api_info.html#ad6fb4c22fd16cfb78b1d183f35d74ce5", null ],
    [ "ImageUrl", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_tmdb_api_info.html#afa8f90a064beeaa2492039c218ffd9a6", null ],
    [ "V3Auth", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_tmdb_api_info.html#a95c7dcedacc913c3f0cfb452bef7efbf", null ],
    [ "V4Auth", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_tmdb_api_info.html#a2752226de2de79bece61cab8d7d31c60", null ]
];