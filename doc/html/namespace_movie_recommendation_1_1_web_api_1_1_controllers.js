var namespace_movie_recommendation_1_1_web_api_1_1_controllers =
[
    [ "AccountController", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_account_controller.html", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_account_controller" ],
    [ "CronMovieApi", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_cron_movie_api.html", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_cron_movie_api" ],
    [ "MoviesController", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_movies_controller.html", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_movies_controller" ],
    [ "WeatherForecastController", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_weather_forecast_controller.html", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_weather_forecast_controller" ]
];