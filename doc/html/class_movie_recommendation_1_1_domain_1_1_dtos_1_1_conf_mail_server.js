var class_movie_recommendation_1_1_domain_1_1_dtos_1_1_conf_mail_server =
[
    [ "CredentialPass", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_conf_mail_server.html#add3e4fd844ae1b669ceecd09e250d7bb", null ],
    [ "CredentialUser", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_conf_mail_server.html#a3543d6d9cec0c9ecdddfc362412f857e", null ],
    [ "EnableSSL", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_conf_mail_server.html#ac971f28a2263261a2e5464df04ee2fbe", null ],
    [ "Host", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_conf_mail_server.html#a2fed303ea6090df82db929b3e69c968e", null ],
    [ "SmtpPort", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_conf_mail_server.html#a66a9e51866e2e36ab30c1826cc9f001b", null ]
];