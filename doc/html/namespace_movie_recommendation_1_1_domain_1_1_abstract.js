var namespace_movie_recommendation_1_1_domain_1_1_abstract =
[
    [ "BaseEntity", "class_movie_recommendation_1_1_domain_1_1_abstract_1_1_base_entity.html", "class_movie_recommendation_1_1_domain_1_1_abstract_1_1_base_entity" ],
    [ "IConfMailServer", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_conf_mail_server.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_conf_mail_server" ],
    [ "IHangfireJobs", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_hangfire_jobs.html", null ],
    [ "IMailTools", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_mail_tools.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_mail_tools" ],
    [ "IMovieRepository", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_movie_repository.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_movie_repository" ],
    [ "IRepository", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository" ],
    [ "ITmdbApiInfo", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_tmdb_api_info.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_tmdb_api_info" ],
    [ "IUserRepository", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_user_repository.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_user_repository" ],
    [ "IVoteRepository", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_vote_repository.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_vote_repository" ]
];