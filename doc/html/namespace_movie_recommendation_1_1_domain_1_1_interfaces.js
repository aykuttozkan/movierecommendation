var namespace_movie_recommendation_1_1_domain_1_1_interfaces =
[
    [ "ICronJobs", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_cron_jobs.html", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_cron_jobs" ],
    [ "IMailService", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_mail_service.html", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_mail_service" ],
    [ "IMovieService", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_movie_service.html", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_movie_service" ],
    [ "IUserService", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_user_service.html", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_user_service" ],
    [ "IVoteService", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_vote_service.html", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_vote_service" ]
];