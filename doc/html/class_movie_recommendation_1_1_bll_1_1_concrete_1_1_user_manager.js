var class_movie_recommendation_1_1_bll_1_1_concrete_1_1_user_manager =
[
    [ "UserManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_user_manager.html#a990a4e7cf8c83821922a878536dc4c4f", null ],
    [ "GetUserAsync", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_user_manager.html#a7b004c98e5ca0aa6e7109f5cfc39e0d9", null ],
    [ "LoginAsync", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_user_manager.html#ae030fd6ea6941e52d429317ed38828a3", null ],
    [ "_UnitOfWork", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_user_manager.html#a5933db8344943e88757d51045cb7988f", null ]
];