var namespace_movie_recommendation_1_1_dal =
[
    [ "Concrete", "namespace_movie_recommendation_1_1_dal_1_1_concrete.html", "namespace_movie_recommendation_1_1_dal_1_1_concrete" ],
    [ "Context", "namespace_movie_recommendation_1_1_dal_1_1_context.html", "namespace_movie_recommendation_1_1_dal_1_1_context" ],
    [ "Migrations", "namespace_movie_recommendation_1_1_dal_1_1_migrations.html", "namespace_movie_recommendation_1_1_dal_1_1_migrations" ],
    [ "UnitOfWork", "class_movie_recommendation_1_1_dal_1_1_unit_of_work.html", "class_movie_recommendation_1_1_dal_1_1_unit_of_work" ]
];