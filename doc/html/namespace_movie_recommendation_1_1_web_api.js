var namespace_movie_recommendation_1_1_web_api =
[
    [ "Controllers", "namespace_movie_recommendation_1_1_web_api_1_1_controllers.html", "namespace_movie_recommendation_1_1_web_api_1_1_controllers" ],
    [ "Filters", "namespace_movie_recommendation_1_1_web_api_1_1_filters.html", "namespace_movie_recommendation_1_1_web_api_1_1_filters" ],
    [ "Hangfire", "namespace_movie_recommendation_1_1_web_api_1_1_hangfire.html", "namespace_movie_recommendation_1_1_web_api_1_1_hangfire" ],
    [ "Program", "class_movie_recommendation_1_1_web_api_1_1_program.html", "class_movie_recommendation_1_1_web_api_1_1_program" ],
    [ "Startup", "class_movie_recommendation_1_1_web_api_1_1_startup.html", "class_movie_recommendation_1_1_web_api_1_1_startup" ],
    [ "WeatherForecast", "class_movie_recommendation_1_1_web_api_1_1_weather_forecast.html", "class_movie_recommendation_1_1_web_api_1_1_weather_forecast" ]
];