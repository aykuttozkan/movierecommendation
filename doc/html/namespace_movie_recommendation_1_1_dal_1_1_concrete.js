var namespace_movie_recommendation_1_1_dal_1_1_concrete =
[
    [ "MovieRepository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_movie_repository.html", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_movie_repository" ],
    [ "Repository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository.html", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository" ],
    [ "UserRepository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_user_repository.html", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_user_repository" ],
    [ "VoteRepository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_vote_repository.html", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_vote_repository" ]
];