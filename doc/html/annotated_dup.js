var annotated_dup =
[
    [ "MovieRecommendation", "namespace_movie_recommendation.html", [
      [ "Bll", "namespace_movie_recommendation_1_1_bll.html", [
        [ "Concrete", "namespace_movie_recommendation_1_1_bll_1_1_concrete.html", [
          [ "CronJobManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager.html", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager" ],
          [ "MailService", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_mail_service.html", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_mail_service" ],
          [ "MovieManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager" ],
          [ "UserManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_user_manager.html", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_user_manager" ],
          [ "VoteManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_vote_manager.html", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_vote_manager" ]
        ] ],
        [ "ServiceCollectionExtentions", "namespace_movie_recommendation_1_1_bll_1_1_service_collection_extentions.html", [
          [ "ServiceCollectionExtentions", "class_movie_recommendation_1_1_bll_1_1_service_collection_extentions_1_1_service_collection_extentions.html", "class_movie_recommendation_1_1_bll_1_1_service_collection_extentions_1_1_service_collection_extentions" ]
        ] ]
      ] ],
      [ "Core", "namespace_movie_recommendation_1_1_core.html", [
        [ "Extentions", "namespace_movie_recommendation_1_1_core_1_1_extentions.html", [
          [ "StringExtentions", "class_movie_recommendation_1_1_core_1_1_extentions_1_1_string_extentions.html", "class_movie_recommendation_1_1_core_1_1_extentions_1_1_string_extentions" ]
        ] ],
        [ "Tools", "namespace_movie_recommendation_1_1_core_1_1_tools.html", [
          [ "EntityModelTools", "class_movie_recommendation_1_1_core_1_1_tools_1_1_entity_model_tools.html", "class_movie_recommendation_1_1_core_1_1_tools_1_1_entity_model_tools" ],
          [ "MailTools", "class_movie_recommendation_1_1_core_1_1_tools_1_1_mail_tools.html", "class_movie_recommendation_1_1_core_1_1_tools_1_1_mail_tools" ],
          [ "ThreadTools", "class_movie_recommendation_1_1_core_1_1_tools_1_1_thread_tools.html", "class_movie_recommendation_1_1_core_1_1_tools_1_1_thread_tools" ]
        ] ]
      ] ],
      [ "Dal", "namespace_movie_recommendation_1_1_dal.html", [
        [ "Concrete", "namespace_movie_recommendation_1_1_dal_1_1_concrete.html", [
          [ "MovieRepository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_movie_repository.html", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_movie_repository" ],
          [ "Repository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository.html", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository" ],
          [ "UserRepository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_user_repository.html", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_user_repository" ],
          [ "VoteRepository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_vote_repository.html", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_vote_repository" ]
        ] ],
        [ "Context", "namespace_movie_recommendation_1_1_dal_1_1_context.html", [
          [ "MovieDbContext", "class_movie_recommendation_1_1_dal_1_1_context_1_1_movie_db_context.html", "class_movie_recommendation_1_1_dal_1_1_context_1_1_movie_db_context" ]
        ] ],
        [ "Migrations", "namespace_movie_recommendation_1_1_dal_1_1_migrations.html", [
          [ "_01_Init", "class_movie_recommendation_1_1_dal_1_1_migrations_1_1__01___init.html", "class_movie_recommendation_1_1_dal_1_1_migrations_1_1__01___init" ],
          [ "_02_addDefaultUser", "class_movie_recommendation_1_1_dal_1_1_migrations_1_1__02__add_default_user.html", "class_movie_recommendation_1_1_dal_1_1_migrations_1_1__02__add_default_user" ],
          [ "MovieDbContextModelSnapshot", "class_movie_recommendation_1_1_dal_1_1_migrations_1_1_movie_db_context_model_snapshot.html", "class_movie_recommendation_1_1_dal_1_1_migrations_1_1_movie_db_context_model_snapshot" ]
        ] ],
        [ "UnitOfWork", "class_movie_recommendation_1_1_dal_1_1_unit_of_work.html", "class_movie_recommendation_1_1_dal_1_1_unit_of_work" ]
      ] ],
      [ "Domain", "namespace_movie_recommendation_1_1_domain.html", [
        [ "Abstract", "namespace_movie_recommendation_1_1_domain_1_1_abstract.html", [
          [ "BaseEntity", "class_movie_recommendation_1_1_domain_1_1_abstract_1_1_base_entity.html", "class_movie_recommendation_1_1_domain_1_1_abstract_1_1_base_entity" ],
          [ "IConfMailServer", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_conf_mail_server.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_conf_mail_server" ],
          [ "IHangfireJobs", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_hangfire_jobs.html", null ],
          [ "IMailTools", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_mail_tools.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_mail_tools" ],
          [ "IMovieRepository", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_movie_repository.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_movie_repository" ],
          [ "IRepository", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository" ],
          [ "ITmdbApiInfo", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_tmdb_api_info.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_tmdb_api_info" ],
          [ "IUserRepository", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_user_repository.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_user_repository" ],
          [ "IVoteRepository", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_vote_repository.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_vote_repository" ]
        ] ],
        [ "Dtos", "namespace_movie_recommendation_1_1_domain_1_1_dtos.html", [
          [ "CommentAndVoteDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_comment_and_vote_dto.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_comment_and_vote_dto" ],
          [ "ConfMailServer", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_conf_mail_server.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_conf_mail_server" ],
          [ "CronJobStatusModel", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_cron_job_status_model.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_cron_job_status_model" ],
          [ "JwtConfigModel", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_jwt_config_model.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_jwt_config_model" ],
          [ "LoginDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_login_dto.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_login_dto" ],
          [ "OperationClaim", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_operation_claim.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_operation_claim" ],
          [ "PaginationDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_pagination_dto.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_pagination_dto" ],
          [ "ResultModel", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_model.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_model" ],
          [ "ResultMovieDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto" ],
          [ "ResultMovieWithPaginationModel", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_with_pagination_model.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_with_pagination_model" ],
          [ "TmdbApiInfo", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_api_info.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_api_info" ],
          [ "TmdbRequestDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_request_dto.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_request_dto" ],
          [ "TmDbResult", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result" ],
          [ "UserResponseDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_user_response_dto.html", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_user_response_dto" ]
        ] ],
        [ "Entities", "namespace_movie_recommendation_1_1_domain_1_1_entities.html", [
          [ "Movie", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie" ],
          [ "User", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_user.html", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_user" ],
          [ "Vote", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_vote.html", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_vote" ]
        ] ],
        [ "Interfaces", "namespace_movie_recommendation_1_1_domain_1_1_interfaces.html", [
          [ "ICronJobs", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_cron_jobs.html", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_cron_jobs" ],
          [ "IMailService", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_mail_service.html", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_mail_service" ],
          [ "IMovieService", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_movie_service.html", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_movie_service" ],
          [ "IUserService", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_user_service.html", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_user_service" ],
          [ "IVoteService", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_vote_service.html", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_vote_service" ]
        ] ],
        [ "IUnitOfWork", "interface_movie_recommendation_1_1_domain_1_1_i_unit_of_work.html", "interface_movie_recommendation_1_1_domain_1_1_i_unit_of_work" ]
      ] ],
      [ "WebApi", "namespace_movie_recommendation_1_1_web_api.html", [
        [ "Controllers", "namespace_movie_recommendation_1_1_web_api_1_1_controllers.html", [
          [ "AccountController", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_account_controller.html", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_account_controller" ],
          [ "CronMovieApi", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_cron_movie_api.html", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_cron_movie_api" ],
          [ "MoviesController", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_movies_controller.html", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_movies_controller" ],
          [ "WeatherForecastController", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_weather_forecast_controller.html", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_weather_forecast_controller" ]
        ] ],
        [ "Filters", "namespace_movie_recommendation_1_1_web_api_1_1_filters.html", [
          [ "ValidateModelAttribute", "class_movie_recommendation_1_1_web_api_1_1_filters_1_1_validate_model_attribute.html", "class_movie_recommendation_1_1_web_api_1_1_filters_1_1_validate_model_attribute" ]
        ] ],
        [ "Hangfire", "namespace_movie_recommendation_1_1_web_api_1_1_hangfire.html", [
          [ "HangfireJobs", "class_movie_recommendation_1_1_web_api_1_1_hangfire_1_1_hangfire_jobs.html", "class_movie_recommendation_1_1_web_api_1_1_hangfire_1_1_hangfire_jobs" ]
        ] ],
        [ "Program", "class_movie_recommendation_1_1_web_api_1_1_program.html", "class_movie_recommendation_1_1_web_api_1_1_program" ],
        [ "Startup", "class_movie_recommendation_1_1_web_api_1_1_startup.html", "class_movie_recommendation_1_1_web_api_1_1_startup" ],
        [ "WeatherForecast", "class_movie_recommendation_1_1_web_api_1_1_weather_forecast.html", "class_movie_recommendation_1_1_web_api_1_1_weather_forecast" ]
      ] ],
      [ "XUnitTest", "namespace_movie_recommendation_1_1_x_unit_test.html", [
        [ "MailToolsTest", "class_movie_recommendation_1_1_x_unit_test_1_1_mail_tools_test.html", "class_movie_recommendation_1_1_x_unit_test_1_1_mail_tools_test" ],
        [ "DependencySetupFixture", "class_movie_recommendation_1_1_x_unit_test_1_1_dependency_setup_fixture.html", "class_movie_recommendation_1_1_x_unit_test_1_1_dependency_setup_fixture" ],
        [ "StringExtentionsTest", "class_movie_recommendation_1_1_x_unit_test_1_1_string_extentions_test.html", "class_movie_recommendation_1_1_x_unit_test_1_1_string_extentions_test" ],
        [ "UserManagerTest", "class_movie_recommendation_1_1_x_unit_test_1_1_user_manager_test.html", "class_movie_recommendation_1_1_x_unit_test_1_1_user_manager_test" ]
      ] ]
    ] ]
];