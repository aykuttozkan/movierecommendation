var dir_8fdf6b2ca446f551ae24a8a2fbab1444 =
[
    [ "Abstract", "dir_2fc955f7b7d51938ca8491d3cc3bcbf5.html", "dir_2fc955f7b7d51938ca8491d3cc3bcbf5" ],
    [ "Dtos", "dir_3e3301ad0f960202d9226560631ef2c1.html", "dir_3e3301ad0f960202d9226560631ef2c1" ],
    [ "Entities", "dir_ed3802b5dff7712906bd9dc1ed3a1a56.html", "dir_ed3802b5dff7712906bd9dc1ed3a1a56" ],
    [ "Interfaces", "dir_e96002f6603aed5ae1baedbc08739705.html", "dir_e96002f6603aed5ae1baedbc08739705" ],
    [ "obj", "dir_79a67d95980864bd4bf8971b419c43a8.html", "dir_79a67d95980864bd4bf8971b419c43a8" ],
    [ "IUnitOfWork.cs", "_i_unit_of_work_8cs.html", [
      [ "IUnitOfWork", "interface_movie_recommendation_1_1_domain_1_1_i_unit_of_work.html", "interface_movie_recommendation_1_1_domain_1_1_i_unit_of_work" ]
    ] ]
];