var namespace_movie_recommendation_1_1_domain_1_1_entities =
[
    [ "Movie", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie" ],
    [ "User", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_user.html", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_user" ],
    [ "Vote", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_vote.html", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_vote" ]
];