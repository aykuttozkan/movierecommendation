var hierarchy =
[
    [ "ActionFilterAttribute", null, [
      [ "MovieRecommendation.WebApi.Filters.ValidateModelAttribute", "class_movie_recommendation_1_1_web_api_1_1_filters_1_1_validate_model_attribute.html", null ]
    ] ],
    [ "MovieRecommendation.Domain.Abstract.BaseEntity< TType >", "class_movie_recommendation_1_1_domain_1_1_abstract_1_1_base_entity.html", null ],
    [ "MovieRecommendation.Domain.Abstract.BaseEntity< int >", "class_movie_recommendation_1_1_domain_1_1_abstract_1_1_base_entity.html", [
      [ "MovieRecommendation.Domain.Entities.Movie", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html", null ],
      [ "MovieRecommendation.Domain.Entities.User", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_user.html", null ],
      [ "MovieRecommendation.Domain.Entities.Vote", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_vote.html", null ]
    ] ],
    [ "MovieRecommendation.Domain.Dtos.CommentAndVoteDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_comment_and_vote_dto.html", null ],
    [ "ControllerBase", null, [
      [ "MovieRecommendation.WebApi.Controllers.AccountController", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_account_controller.html", null ],
      [ "MovieRecommendation.WebApi.Controllers.CronMovieApi", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_cron_movie_api.html", null ],
      [ "MovieRecommendation.WebApi.Controllers.MoviesController", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_movies_controller.html", null ],
      [ "MovieRecommendation.WebApi.Controllers.WeatherForecastController", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_weather_forecast_controller.html", null ]
    ] ],
    [ "MovieRecommendation.Domain.Dtos.CronJobStatusModel", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_cron_job_status_model.html", null ],
    [ "DbContext", null, [
      [ "MovieRecommendation.Dal.Context.MovieDbContext", "class_movie_recommendation_1_1_dal_1_1_context_1_1_movie_db_context.html", null ]
    ] ],
    [ "MovieRecommendation.XUnitTest.DependencySetupFixture", "class_movie_recommendation_1_1_x_unit_test_1_1_dependency_setup_fixture.html", null ],
    [ "MovieRecommendation.Core.Tools.EntityModelTools", "class_movie_recommendation_1_1_core_1_1_tools_1_1_entity_model_tools.html", null ],
    [ "IClassFixture", null, [
      [ "MovieRecommendation.XUnitTest.MailToolsTest", "class_movie_recommendation_1_1_x_unit_test_1_1_mail_tools_test.html", null ]
    ] ],
    [ "MovieRecommendation.Domain.Abstract.IConfMailServer", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_conf_mail_server.html", [
      [ "MovieRecommendation.Domain.Dtos.ConfMailServer", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_conf_mail_server.html", null ]
    ] ],
    [ "MovieRecommendation.Domain.Interfaces.ICronJobs", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_cron_jobs.html", [
      [ "MovieRecommendation.Bll.Concrete.CronJobManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_cron_job_manager.html", null ]
    ] ],
    [ "IDisposable", null, [
      [ "MovieRecommendation.Domain.IUnitOfWork", "interface_movie_recommendation_1_1_domain_1_1_i_unit_of_work.html", [
        [ "MovieRecommendation.Dal.UnitOfWork", "class_movie_recommendation_1_1_dal_1_1_unit_of_work.html", null ]
      ] ]
    ] ],
    [ "MovieRecommendation.Domain.Abstract.IHangfireJobs", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_hangfire_jobs.html", [
      [ "MovieRecommendation.WebApi.Hangfire.HangfireJobs", "class_movie_recommendation_1_1_web_api_1_1_hangfire_1_1_hangfire_jobs.html", null ]
    ] ],
    [ "MovieRecommendation.Domain.Interfaces.IMailService", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_mail_service.html", [
      [ "MovieRecommendation.Bll.Concrete.MailService", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_mail_service.html", null ]
    ] ],
    [ "MovieRecommendation.Domain.Abstract.IMailTools", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_mail_tools.html", [
      [ "MovieRecommendation.Core.Tools.MailTools", "class_movie_recommendation_1_1_core_1_1_tools_1_1_mail_tools.html", null ]
    ] ],
    [ "MovieRecommendation.Domain.Interfaces.IMovieService", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_movie_service.html", [
      [ "MovieRecommendation.Bll.Concrete.MovieManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html", null ]
    ] ],
    [ "MovieRecommendation.Domain.Abstract.IRepository< TEntity >", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository.html", [
      [ "MovieRecommendation.Dal.Concrete.Repository< TEntity >", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository.html", null ]
    ] ],
    [ "MovieRecommendation.Domain.Abstract.IRepository< Movie >", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository.html", [
      [ "MovieRecommendation.Domain.Abstract.IMovieRepository", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_movie_repository.html", [
        [ "MovieRecommendation.Dal.Concrete.MovieRepository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_movie_repository.html", null ]
      ] ]
    ] ],
    [ "MovieRecommendation.Domain.Abstract.IRepository< User >", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository.html", [
      [ "MovieRecommendation.Domain.Abstract.IUserRepository", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_user_repository.html", [
        [ "MovieRecommendation.Dal.Concrete.UserRepository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_user_repository.html", null ]
      ] ]
    ] ],
    [ "MovieRecommendation.Domain.Abstract.IRepository< Vote >", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository.html", [
      [ "MovieRecommendation.Domain.Abstract.IVoteRepository", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_vote_repository.html", [
        [ "MovieRecommendation.Dal.Concrete.VoteRepository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_vote_repository.html", null ]
      ] ]
    ] ],
    [ "MovieRecommendation.Domain.Abstract.ITmdbApiInfo", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_tmdb_api_info.html", [
      [ "MovieRecommendation.Domain.Dtos.TmdbApiInfo", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_api_info.html", null ]
    ] ],
    [ "MovieRecommendation.Domain.Interfaces.IUserService", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_user_service.html", [
      [ "MovieRecommendation.Bll.Concrete.UserManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_user_manager.html", null ]
    ] ],
    [ "MovieRecommendation.Domain.Interfaces.IVoteService", "interface_movie_recommendation_1_1_domain_1_1_interfaces_1_1_i_vote_service.html", [
      [ "MovieRecommendation.Bll.Concrete.VoteManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_vote_manager.html", null ]
    ] ],
    [ "MovieRecommendation.Domain.Dtos.JwtConfigModel", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_jwt_config_model.html", null ],
    [ "MovieRecommendation.Domain.Dtos.LoginDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_login_dto.html", null ],
    [ "Migration", null, [
      [ "MovieRecommendation.Dal.Migrations._01_Init", "class_movie_recommendation_1_1_dal_1_1_migrations_1_1__01___init.html", null ],
      [ "MovieRecommendation.Dal.Migrations._02_addDefaultUser", "class_movie_recommendation_1_1_dal_1_1_migrations_1_1__02__add_default_user.html", null ]
    ] ],
    [ "ModelSnapshot", null, [
      [ "MovieRecommendation.Dal.Migrations.MovieDbContextModelSnapshot", "class_movie_recommendation_1_1_dal_1_1_migrations_1_1_movie_db_context_model_snapshot.html", null ]
    ] ],
    [ "MovieRecommendation.Domain.Dtos.OperationClaim", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_operation_claim.html", null ],
    [ "MovieRecommendation.Domain.Dtos.PaginationDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_pagination_dto.html", null ],
    [ "MovieRecommendation.WebApi.Program", "class_movie_recommendation_1_1_web_api_1_1_program.html", null ],
    [ "MovieRecommendation.Dal.Concrete.Repository< Movie >", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository.html", [
      [ "MovieRecommendation.Dal.Concrete.MovieRepository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_movie_repository.html", null ]
    ] ],
    [ "MovieRecommendation.Dal.Concrete.Repository< User >", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository.html", [
      [ "MovieRecommendation.Dal.Concrete.UserRepository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_user_repository.html", null ]
    ] ],
    [ "MovieRecommendation.Dal.Concrete.Repository< Vote >", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository.html", [
      [ "MovieRecommendation.Dal.Concrete.VoteRepository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_vote_repository.html", null ]
    ] ],
    [ "MovieRecommendation.Domain.Dtos.ResultModel", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_model.html", null ],
    [ "MovieRecommendation.Domain.Dtos.ResultMovieDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html", null ],
    [ "MovieRecommendation.Domain.Dtos.ResultMovieWithPaginationModel", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_with_pagination_model.html", null ],
    [ "MovieRecommendation.Bll.ServiceCollectionExtentions.ServiceCollectionExtentions", "class_movie_recommendation_1_1_bll_1_1_service_collection_extentions_1_1_service_collection_extentions.html", null ],
    [ "MovieRecommendation.WebApi.Startup", "class_movie_recommendation_1_1_web_api_1_1_startup.html", null ],
    [ "MovieRecommendation.Core.Extentions.StringExtentions", "class_movie_recommendation_1_1_core_1_1_extentions_1_1_string_extentions.html", null ],
    [ "MovieRecommendation.XUnitTest.StringExtentionsTest", "class_movie_recommendation_1_1_x_unit_test_1_1_string_extentions_test.html", null ],
    [ "MovieRecommendation.Core.Tools.ThreadTools", "class_movie_recommendation_1_1_core_1_1_tools_1_1_thread_tools.html", null ],
    [ "MovieRecommendation.Domain.Dtos.TmdbRequestDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tmdb_request_dto.html", null ],
    [ "MovieRecommendation.Domain.Dtos.TmDbResult", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html", null ],
    [ "MovieRecommendation.XUnitTest.UserManagerTest", "class_movie_recommendation_1_1_x_unit_test_1_1_user_manager_test.html", null ],
    [ "MovieRecommendation.Domain.Dtos.UserResponseDto", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_user_response_dto.html", null ],
    [ "MovieRecommendation.WebApi.WeatherForecast", "class_movie_recommendation_1_1_web_api_1_1_weather_forecast.html", null ]
];