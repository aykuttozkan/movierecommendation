var class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository =
[
    [ "Repository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository.html#a8f94f3eb3379b0ca5cb615db266caa6b", null ],
    [ "AddAsync", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository.html#ab4104b5d941a13010f56304bc3a0e0b4", null ],
    [ "AddRangeAsync", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository.html#a381f0c61ee973c74c278b07c97871078", null ],
    [ "Find", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository.html#a5a5c3e4bc29971fc46e314fa3ee4a42a", null ],
    [ "FindQueryable", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository.html#a9fccae68a96b69621ce53e2b502904cc", null ],
    [ "FirstOrDefaultAsync", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository.html#a952b039371b0a3b95ba7b33ad6a0789d", null ],
    [ "GetAllAsync", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository.html#a3ed716db7d76707288c36f4848539658", null ],
    [ "GetByIdAsync", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository.html#a1a52eb9300f4613992bf52adf5177cc1", null ],
    [ "Remove", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository.html#a068fb2d073b5b4a7bd717956f99ab58c", null ],
    [ "RemoveRange", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository.html#ad17c665e5ea5727f903078fd05af8296", null ],
    [ "Context", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository.html#a8da352f9f379187ba040aa370811e09e", null ]
];