var class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_movies_controller =
[
    [ "MoviesController", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_movies_controller.html#afc5c02a13b494467e496329520f1cacd", null ],
    [ "GetMovie", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_movies_controller.html#a91f8a204af158d7b216cac06096075e1", null ],
    [ "GetMovieWithPagination", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_movies_controller.html#ae916a0fefa44af480d0c46b3380861fe", null ],
    [ "PostAddCommitAndVote", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_movies_controller.html#adbd78d2e71e70493522db4b8243464b3", null ],
    [ "PostMovieRecommend", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_movies_controller.html#a3e169b5b589d5d4de41ef0b7cec0d68e", null ],
    [ "Logger", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_movies_controller.html#a82d4698313f7f2dc7ec6771bb4f16aca", null ],
    [ "MovieService", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_movies_controller.html#ad83065d3dcb68bb6874141c679873bc4", null ],
    [ "VoteService", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_movies_controller.html#a6a71b36a442d030413415e7dcbcc76b7", null ]
];