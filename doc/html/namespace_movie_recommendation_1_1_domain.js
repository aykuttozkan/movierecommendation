var namespace_movie_recommendation_1_1_domain =
[
    [ "Abstract", "namespace_movie_recommendation_1_1_domain_1_1_abstract.html", "namespace_movie_recommendation_1_1_domain_1_1_abstract" ],
    [ "Dtos", "namespace_movie_recommendation_1_1_domain_1_1_dtos.html", "namespace_movie_recommendation_1_1_domain_1_1_dtos" ],
    [ "Entities", "namespace_movie_recommendation_1_1_domain_1_1_entities.html", "namespace_movie_recommendation_1_1_domain_1_1_entities" ],
    [ "Interfaces", "namespace_movie_recommendation_1_1_domain_1_1_interfaces.html", "namespace_movie_recommendation_1_1_domain_1_1_interfaces" ],
    [ "IUnitOfWork", "interface_movie_recommendation_1_1_domain_1_1_i_unit_of_work.html", "interface_movie_recommendation_1_1_domain_1_1_i_unit_of_work" ]
];