var dir_ed69c21df05212bf5d97c60f13cb0d1e =
[
    [ "MovieRecommendation.Bll", "dir_047926727b23ccda1791bd8ac7e3cd44.html", "dir_047926727b23ccda1791bd8ac7e3cd44" ],
    [ "MovieRecommendation.Core", "dir_fd5cf6919733c96e9563019cf915d02c.html", "dir_fd5cf6919733c96e9563019cf915d02c" ],
    [ "MovieRecommendation.Dal", "dir_335aaa162b21727f330601c39c0886e9.html", "dir_335aaa162b21727f330601c39c0886e9" ],
    [ "MovieRecommendation.Domain", "dir_8fdf6b2ca446f551ae24a8a2fbab1444.html", "dir_8fdf6b2ca446f551ae24a8a2fbab1444" ],
    [ "MovieRecommendation.WebApi", "dir_9321eac68c7af312dd561db121ede1f7.html", "dir_9321eac68c7af312dd561db121ede1f7" ],
    [ "MovieRecommendation.XUnitTest", "dir_699a91d57037d1f2648f82cb20062eeb.html", "dir_699a91d57037d1f2648f82cb20062eeb" ]
];