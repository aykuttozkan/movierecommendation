var class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager =
[
    [ "MovieManager", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html#a9d9af7326ccd3e7d24c8c435ed91e20e", null ],
    [ "CreateMovie", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html#a90d4ccc5ad77be2f42fd47c23ba6b78e", null ],
    [ "DeleteMovie", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html#a90a0a133293edd94d37787038cee42ec", null ],
    [ "GetAllMovies", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html#a955557f5b18d67c557b2348b62e5424d", null ],
    [ "GetAllMoviesByPageFilter", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html#a558f6153c0df95d0de142ef1dcfe5e91", null ],
    [ "GetMovieById", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html#adcd43f4157e9b3064d6c0c8ff5da42d1", null ],
    [ "GetMovieBySyncId", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html#aacb9ff960b5ead6c2a896cc6a04fbac6", null ],
    [ "Recommend", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html#a8b515a1d16d9e77b68baec4cfcd2a41a", null ],
    [ "UpdateMovie", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html#a78ef4c8194240d4c0d7dfa5431a5e3f2", null ],
    [ "_MailTools", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html#aba034a2832b77ad48fc3f9546eaa4d66", null ],
    [ "_TmdbApiInfo", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html#a27067d87938485c0a0876e31cd614d52", null ],
    [ "_UnitOfWork", "class_movie_recommendation_1_1_bll_1_1_concrete_1_1_movie_manager.html#a2269e94d55a9ba95a96f6e16f608e2e3", null ]
];