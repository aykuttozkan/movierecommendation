var class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto =
[
    [ "BackdropPath", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html#ac942f0258fd861c8a449f4dbff367560", null ],
    [ "Id", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html#af1637bf282211acfed162333df017555", null ],
    [ "OriginalLanguage", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html#a928316481ae5bf1369690db6a6ee89ad", null ],
    [ "OriginalTitle", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html#aed4b593b48f25d4cdfa2285ccac62ba7", null ],
    [ "Overview", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html#ab6b67698ace29d69f68478588195dc96", null ],
    [ "Popularity", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html#aff5eefd9bd068483f155e90c2d001844", null ],
    [ "PosterPath", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html#adf8583d4c350f21ef9c9ffedb5a10c20", null ],
    [ "ReleaseDate", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html#a47ee6b5a9a507648e4403b6fe6c3fdb2", null ],
    [ "SyncId", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html#a7d2572f8034b8c2de2676aa7bbedee17", null ],
    [ "Title", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html#a88176c23e9e3902d786e51445fb12661", null ],
    [ "Video", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html#a6f7ba88fb99ad6c6f873a0e58bac19cf", null ],
    [ "Vote", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html#a9e61d43d28399d58773d661c31d00a65", null ],
    [ "VoteAverage", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html#ac297aadea42f25fca1582dd62ed30436", null ],
    [ "VoteCount", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html#a08014c190256d994e8dc3ca4eb2eda86", null ],
    [ "VoteNote", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html#a179c22b81716fdba98cb07dc743f7762", null ],
    [ "VoteValue", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_result_movie_dto.html#a285bd104305f89b54d674dff2aee1e21", null ]
];