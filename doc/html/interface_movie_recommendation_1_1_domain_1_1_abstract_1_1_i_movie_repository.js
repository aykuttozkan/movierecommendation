var interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_movie_repository =
[
    [ "GetAllWithMovieAsync", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_movie_repository.html#a95a7cf065fcecac70649ca91ccea8529", null ],
    [ "GetAllWithMovieByMovieIdAsync", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_movie_repository.html#afb01e127d81c833a936574eb0887503e", null ],
    [ "GetWithMovieByIdAsync", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_movie_repository.html#ac745d0f52a49648f17ec1008ce013989", null ],
    [ "GetWithMovieBySyncIdAsync", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_movie_repository.html#abc3b09fdaf2cb3f610d70e33f4d6946b", null ]
];