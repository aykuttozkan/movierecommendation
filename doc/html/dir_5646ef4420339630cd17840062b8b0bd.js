var dir_5646ef4420339630cd17840062b8b0bd =
[
    [ "AccountController.cs", "_account_controller_8cs.html", [
      [ "AccountController", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_account_controller.html", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_account_controller" ]
    ] ],
    [ "CronMovieApi.cs", "_cron_movie_api_8cs.html", [
      [ "CronMovieApi", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_cron_movie_api.html", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_cron_movie_api" ]
    ] ],
    [ "MoviesController.cs", "_movies_controller_8cs.html", [
      [ "MoviesController", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_movies_controller.html", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_movies_controller" ]
    ] ],
    [ "WeatherForecastController.cs", "_weather_forecast_controller_8cs.html", [
      [ "WeatherForecastController", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_weather_forecast_controller.html", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_weather_forecast_controller" ]
    ] ]
];