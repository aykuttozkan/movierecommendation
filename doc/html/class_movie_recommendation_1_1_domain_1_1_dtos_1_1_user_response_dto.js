var class_movie_recommendation_1_1_domain_1_1_dtos_1_1_user_response_dto =
[
    [ "Email", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_user_response_dto.html#a11249b3dc93ea779244a2d204874cc5d", null ],
    [ "Guid", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_user_response_dto.html#aa3c24e86cbb7dacfe3e1b03d8d6193c7", null ],
    [ "Name", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_user_response_dto.html#a084b0120210364d849da58b31187d6c0", null ],
    [ "Surname", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_user_response_dto.html#a730204971bcf48af544d35acb38fb04f", null ],
    [ "Token", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_user_response_dto.html#a4dde11e5501ff8cd8d2ddbac2597c3f2", null ],
    [ "UserId", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_user_response_dto.html#a939b4b5f50ec3331267e0f0ddb1ba3fd", null ],
    [ "UserName", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_user_response_dto.html#a6085d342581bfc20eb99f2ba9a396e6f", null ]
];