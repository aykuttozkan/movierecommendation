var interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository =
[
    [ "AddAsync", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository.html#aa76d0da0d2b6b7e3a88fcb70baa0ed60", null ],
    [ "AddRangeAsync", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository.html#aa7435b40da7cb0595bf5c8cf2b6916b4", null ],
    [ "Find", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository.html#a6ab8d9474feb34288bea6460e9cf9bd5", null ],
    [ "FindQueryable", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository.html#aef48cab65c8964b9ec3c0f64273c9e35", null ],
    [ "FirstOrDefaultAsync", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository.html#aab5239a39c5ce52813b24b0e5f32c75f", null ],
    [ "GetAllAsync", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository.html#a77a21b706649e4f3078f78caa481c8b4", null ],
    [ "GetByIdAsync", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository.html#a7c1ab9bfcf60aaa7ba37bbb5d5d89690", null ],
    [ "Remove", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository.html#a55979dd883f4fdc1605d388ef2390618", null ],
    [ "RemoveRange", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository.html#a9afd32da1c88df77659a240c0e1da83d", null ]
];