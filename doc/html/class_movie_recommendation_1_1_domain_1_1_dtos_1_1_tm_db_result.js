var class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result =
[
    [ "adult", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html#ab05710c672ebc6ac7d51496f4d6472e4", null ],
    [ "backdrop_path", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html#acac3b2797ff9b775939c6825c2b3cd29", null ],
    [ "id", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html#a63e448c5e6134438fa0554d0b594e465", null ],
    [ "original_language", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html#a5f29a2835254022be4a33b90429c52be", null ],
    [ "original_title", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html#a029d6674f9223299131e949f8f988eff", null ],
    [ "overview", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html#a0c39152615e12c472d7ce8d5bf1ada7d", null ],
    [ "popularity", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html#aff6f618c6a7cc2cff253e12110fd37b6", null ],
    [ "poster_path", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html#aab2f77c5b28e5bcc000a6092df75d216", null ],
    [ "release_date", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html#a7f45813352a3b6aa70d69f6f9cd6a862", null ],
    [ "title", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html#a47dd5a0b53fc2fd7b5946999519055d6", null ],
    [ "video", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html#a0b770e5815383c609e800ef91a6d46b7", null ],
    [ "vote_average", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html#a51677ebf0b5a9e302108224725244388", null ],
    [ "vote_count", "class_movie_recommendation_1_1_domain_1_1_dtos_1_1_tm_db_result.html#a9a1bed02b727740096d2eaf22664178e", null ]
];