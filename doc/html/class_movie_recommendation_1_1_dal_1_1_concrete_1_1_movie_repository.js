var class_movie_recommendation_1_1_dal_1_1_concrete_1_1_movie_repository =
[
    [ "MovieRepository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_movie_repository.html#af274d472b3427a50d98d1e0ece4eee45", null ],
    [ "GetAllWithMovieAsync", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_movie_repository.html#ab9bb520ebb5dff1f988b06c2e2652e03", null ],
    [ "GetAllWithMovieByMovieIdAsync", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_movie_repository.html#ae08c01b46bc9a95ec97096e46dfe49fd", null ],
    [ "GetWithMovieByIdAsync", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_movie_repository.html#a40516e0ce92c25d8f685e3048ab8b0f4", null ],
    [ "GetWithMovieBySyncIdAsync", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_movie_repository.html#ab9db48587c1161f2afac3b81289e1ea9", null ],
    [ "SQLiteDbContext", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_movie_repository.html#ab89617fe667145aa1f1f324f23d1f2bc", null ]
];