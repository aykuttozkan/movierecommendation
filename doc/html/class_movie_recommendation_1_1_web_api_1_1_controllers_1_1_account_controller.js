var class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_account_controller =
[
    [ "AccountController", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_account_controller.html#aa53c8d17c93938e69c3b75e16d5bdf9e", null ],
    [ "GetUserInfo", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_account_controller.html#afdd0f6f3d2bcc5f9f60230050e899ee2", null ],
    [ "Login", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_account_controller.html#a1987b3b3cac1f4fb2462b7a159f49aa5", null ],
    [ "SetClaims", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_account_controller.html#ab0774693d1888a76b776701872edac09", null ],
    [ "_JwtConfigModel", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_account_controller.html#ae913b8e14bbfc0772d3009405528e152", null ],
    [ "Logger", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_account_controller.html#a063479dffcbfc102eb0f2cb3eefa447a", null ],
    [ "UserService", "class_movie_recommendation_1_1_web_api_1_1_controllers_1_1_account_controller.html#a6342c47fd5217319aeec83b1c5c028ef", null ]
];