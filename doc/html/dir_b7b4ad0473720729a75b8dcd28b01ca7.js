var dir_b7b4ad0473720729a75b8dcd28b01ca7 =
[
    [ "MovieRepository.cs", "_movie_repository_8cs.html", [
      [ "MovieRepository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_movie_repository.html", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_movie_repository" ]
    ] ],
    [ "Repository.cs", "_repository_8cs.html", [
      [ "Repository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository.html", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_repository" ]
    ] ],
    [ "UserRepository.cs", "_user_repository_8cs.html", [
      [ "UserRepository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_user_repository.html", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_user_repository" ]
    ] ],
    [ "VoteRepository.cs", "_vote_repository_8cs.html", [
      [ "VoteRepository", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_vote_repository.html", "class_movie_recommendation_1_1_dal_1_1_concrete_1_1_vote_repository" ]
    ] ]
];