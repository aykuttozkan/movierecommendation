var class_movie_recommendation_1_1_dal_1_1_context_1_1_movie_db_context =
[
    [ "MovieDbContext", "class_movie_recommendation_1_1_dal_1_1_context_1_1_movie_db_context.html#a315b57c3e25288d26d0d24f01da65129", null ],
    [ "OnModelCreating", "class_movie_recommendation_1_1_dal_1_1_context_1_1_movie_db_context.html#a23feb4d3e914ab09235e330f0609a02b", null ],
    [ "Movies", "class_movie_recommendation_1_1_dal_1_1_context_1_1_movie_db_context.html#af3f7021695fb75541413a69c0dceccb1", null ],
    [ "Users", "class_movie_recommendation_1_1_dal_1_1_context_1_1_movie_db_context.html#a0a547c15510eab03cd847f87f985441c", null ],
    [ "Votes", "class_movie_recommendation_1_1_dal_1_1_context_1_1_movie_db_context.html#a8a79f50e65284a77fe538bf26f931a38", null ]
];