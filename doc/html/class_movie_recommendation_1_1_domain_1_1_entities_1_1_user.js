var class_movie_recommendation_1_1_domain_1_1_entities_1_1_user =
[
    [ "Email", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_user.html#adcf25ed8c77367d593a01a2a03a30a48", null ],
    [ "Guid", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_user.html#a64c1703888321347761d8f1f5c74b4e8", null ],
    [ "Name", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_user.html#a87d58b52427584bafca5f3417b289e23", null ],
    [ "Password", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_user.html#af96c8ae947645860dc9e6439587bfd94", null ],
    [ "Surname", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_user.html#a592a9c8fa94a2624b2741c6dfebcc139", null ],
    [ "Token", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_user.html#ab9276efa771c457c0b2090529f14ded4", null ],
    [ "UserName", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_user.html#aa8b0aefd8d9de73b928324af112f52c7", null ]
];