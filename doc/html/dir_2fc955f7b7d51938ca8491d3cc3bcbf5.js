var dir_2fc955f7b7d51938ca8491d3cc3bcbf5 =
[
    [ "BaseEntity.cs", "_base_entity_8cs.html", [
      [ "BaseEntity", "class_movie_recommendation_1_1_domain_1_1_abstract_1_1_base_entity.html", "class_movie_recommendation_1_1_domain_1_1_abstract_1_1_base_entity" ]
    ] ],
    [ "IConfMailServer.cs", "_i_conf_mail_server_8cs.html", [
      [ "IConfMailServer", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_conf_mail_server.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_conf_mail_server" ]
    ] ],
    [ "IHangfireJobs.cs", "_i_hangfire_jobs_8cs.html", [
      [ "IHangfireJobs", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_hangfire_jobs.html", null ]
    ] ],
    [ "IMailTools.cs", "_i_mail_tools_8cs.html", [
      [ "IMailTools", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_mail_tools.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_mail_tools" ]
    ] ],
    [ "IMovieRepository.cs", "_i_movie_repository_8cs.html", [
      [ "IMovieRepository", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_movie_repository.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_movie_repository" ]
    ] ],
    [ "IRepository.cs", "_i_repository_8cs.html", [
      [ "IRepository", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_repository" ]
    ] ],
    [ "ITmdbApiInfo.cs", "_i_tmdb_api_info_8cs.html", [
      [ "ITmdbApiInfo", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_tmdb_api_info.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_tmdb_api_info" ]
    ] ],
    [ "IUserRepository.cs", "_i_user_repository_8cs.html", [
      [ "IUserRepository", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_user_repository.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_user_repository" ]
    ] ],
    [ "IVoteRepository.cs", "_i_vote_repository_8cs.html", [
      [ "IVoteRepository", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_vote_repository.html", "interface_movie_recommendation_1_1_domain_1_1_abstract_1_1_i_vote_repository" ]
    ] ]
];