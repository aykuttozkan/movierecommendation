var class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie =
[
    [ "Movie", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html#a5460c1c5aa3b138f3343c8c6cec9bd96", null ],
    [ "BackdropPath", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html#aa20f6e6da3adb8378744c1b5aee6ff4e", null ],
    [ "IsActive", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html#a25585f510cea4160e01d6604876c5f9d", null ],
    [ "IsDelete", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html#a15e453555d72f97ecba6f8490a261887", null ],
    [ "OriginalLanguage", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html#a722c137f65a1a578e1439b13cfe721c9", null ],
    [ "OriginalTitle", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html#a8c3c31d22187a7c993d46467e58548af", null ],
    [ "Overview", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html#ab7ea4b290a71417ae19397194e30b9e2", null ],
    [ "Popularity", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html#adbe34b3f89f637305e13c238b0b239c6", null ],
    [ "PosterPath", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html#af20a6fe8e7cc77c483e5d267d229c131", null ],
    [ "ReleaseDate", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html#ae75ddefbf184c7189b602237a4e36d9f", null ],
    [ "SyncId", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html#af1b8ac659c63c59c0269dd687b4b1b08", null ],
    [ "Title", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html#a76ee46ceb6bfa248803ede6dea5031fd", null ],
    [ "UserId", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html#ae266ecc294ef7e0c6e6a1d9441319f6e", null ],
    [ "Video", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html#a451d2e2bffa506c3d60dfbf7ea27de80", null ],
    [ "Votes", "class_movie_recommendation_1_1_domain_1_1_entities_1_1_movie.html#a5c0bb4362f3d0af67bfc6c1f9b9c2c99", null ]
];