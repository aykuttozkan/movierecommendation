﻿using Microsoft.EntityFrameworkCore;
using MovieRecommendation.Core.Tools;
using MovieRecommendation.Domain;
using MovieRecommendation.Domain.Abstract;
using MovieRecommendation.Domain.Dtos;
using MovieRecommendation.Domain.Entities;
using MovieRecommendation.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieRecommendation.Bll.Concrete
{
    public class MovieManager : IMovieService
    {
        //private readonly IMovieRepository _MovieRepository;
        private readonly IUnitOfWork _UnitOfWork;
        private readonly ITmdbApiInfo _TmdbApiInfo;
        private readonly IMailTools _MailTools;

        public MovieManager(IUnitOfWork unitOfWork, ITmdbApiInfo tmdbApiInfo, IMailTools mailTools)
        {
            //_MovieRepository = movieRepository;
            _UnitOfWork = unitOfWork;
            _TmdbApiInfo = tmdbApiInfo;
            _MailTools = mailTools;
        }

        /// <summary>
        /// Yeni bir film eklemek icin kullanilir
        /// </summary>
        /// <param name="newMovie"></param>
        /// <returns></returns>
        public async Task<Movie> CreateMovie(Movie newMovie)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Film silmek icin kullaniliyor
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        public async Task DeleteMovie(Movie movie)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Tum film bilgilerini veritabanindan almak icin kullaniliyor
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Movie>> GetAllMovies() 
            => await _UnitOfWork.Movies.GetAllAsync();
        
        /// <summary>
        /// Film listeleme islemlerinin yapilabiledigi method. Listeleme islemi icin sayfalama ozelligi olup
        /// <seealso cref="PaginationDto"/> nesnesine gore filtreleme yapilmaktadir
        /// </summary>
        /// <param name="paginationDto"></param>
        /// <returns></returns>
        public async Task<ResultMovieWithPaginationModel> GetAllMoviesByPageFilter(PaginationDto paginationDto)
        {
            var _result = new ResultMovieWithPaginationModel();

            if (paginationDto == null)
                return null;

            // sayfalama islemi soncesi gerekli kontroller yapilarak default degerlerin
            // atamasini yapiyoruz
            paginationDto.Page = paginationDto.Page <= 1 ? 0 : paginationDto.Page;
            paginationDto.ItemPerPage = paginationDto.ItemPerPage <= 0 ? 10 : paginationDto.ItemPerPage;

            var _pageCalc = paginationDto.Page > 1 ? (paginationDto.Page-1) * paginationDto.ItemPerPage : paginationDto.Page * paginationDto.ItemPerPage;

            var _movies = _UnitOfWork
                            .Movies
                                .FindQueryable(x => x.IsActive == true);
            
            // siralama ozelligi ekliyoruz
            _movies = paginationDto.OrderBy == "desc" ? _movies.OrderByDescending(x => x.Id) : _movies.OrderBy(x => x.Id);

            var _totalCount = _movies?.Count() ?? 0;
            var _paginationData = await _movies?
                                        .Skip(_pageCalc)
                                            .Take(paginationDto.ItemPerPage)
                                            .Include(i=> i.Votes)
                                                .Select(s => s.ToDto(_TmdbApiInfo))
                                                    .ToListAsync();

            _result.TotalPage = _totalCount > 0 ? (int)Math.Ceiling((double)_totalCount / (double)paginationDto.ItemPerPage)  : 0;
            _result.Page = paginationDto.Page < 1 ? 1 : paginationDto.Page;
            _result.TotalItem = _totalCount;
            _result.Movies = _paginationData;

            return _result;
        }

        /// <summary>
        /// Film id bilgisine göre getirilen film için, userId bilgisi baz alınarak, kullanıcın
        /// filme yapmış olduğu yorum ve puanlama bilgisini geriye döndürür
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public async Task<ResultMovieDto> GetMovieById(int id, int userID)
        {
            var _movie = await _UnitOfWork.Movies.GetWithMovieByIdAsync(id);
            var _vote = _movie?.Votes.FirstOrDefault(x => x.UserId == userID);
            
            var _result = _movie.ToDto(_TmdbApiInfo);
            _result.Vote = _vote;
            
            return _result;
        }

        /// <summary>
        /// Senkronizasyon id bilgisine göre ilgili film bilgisini geriye döndürür
        /// </summary>
        /// <param name="syncId"></param>
        /// <returns></returns>
        public async Task<ResultMovieDto> GetMovieBySyncId(int syncId)
        {
            var _movie = await _UnitOfWork.Movies.GetWithMovieBySyncIdAsync(syncId);
            
            var _result = _movie.ToDto(_TmdbApiInfo);

            return _result;
        }

        /// <summary>
        /// Mevcutta ki filmi guncellemek icin kullaniliyor
        /// </summary>
        /// <param name="movieToBeUpdated"></param>
        /// <param name="movie"></param>
        /// <returns></returns>
        public async Task UpdateMovie(Movie movieToBeUpdated, Movie movie)
        {
            movieToBeUpdated.Title = movie.Title;
            movieToBeUpdated.Overview = movie.Overview;

            await _UnitOfWork.CommitAsync(true);
        }

        /// <summary>
        /// Film tavsiyesi
        /// </summary>
        /// <param name="movieID"></param>
        /// <param name="userID"></param>
        /// <param name="toMail"></param>
        /// <returns></returns>
        public async Task<bool> Recommend(int movieID, int userID, string toMail)
        {
            var _movie = await _UnitOfWork.Movies.FirstOrDefaultAsync(x => x.Id == movieID);
            var _user = await _UnitOfWork.Users.FirstOrDefaultAsync(x => x.Id == userID);

            string _mailContent = $@"
<html>
<body>
    <h2>Bu filmi mutlaka izlemelisin</h2>
    <p>
        <b>{_user.Name??string.Empty} {_user.Surname??string.Empty}</b> adlı kişi;
        <br/>
        {_movie.Title} filmini izlemenizi öneriyor.
    </p>
    <p>Gönderim tarihi: {DateTime.Now}</p>
</body>
</html>
";
            var _sendMail = await _MailTools.SendEmailAsync(toMail, "Film Tavsiyesi", _mailContent);

            return _sendMail;
        }
    }
}
