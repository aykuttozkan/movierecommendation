﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MovieRecommendation.Domain;
using MovieRecommendation.Domain.Abstract;
using MovieRecommendation.Domain.Dtos;
using MovieRecommendation.Domain.Entities;
using MovieRecommendation.Domain.Interfaces;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;

namespace MovieRecommendation.Bll.Concrete
{
    public class CronJobManager : ICronJobs
    {
        private readonly ITmdbApiInfo TmdbApiInfo;

        private static bool IsRunning = false;

        private static ResultModel ResultModelInfo;

        private static int CounterActiveSync = 0;

        private readonly IConfiguration Configuration;
        private readonly IServiceProvider ServiceProvider;

        public CronJobManager(IServiceProvider serviceProvider, IConfiguration configuration, ITmdbApiInfo tmdbApiInfo)
        {
            ServiceProvider = serviceProvider;
            Configuration = configuration;
            TmdbApiInfo = tmdbApiInfo;

            if (ResultModelInfo == null)
                ResultModelInfo = new ResultModel();
        }

        /// <summary>
        /// Mevcutta calisan sync bilgilerini dondurur
        /// </summary>
        /// <returns></returns>
        public async Task<ResultModel> GetSyncStatusAsync()
        {
            if (IsRunning)
            {
                var _statusDetail = ResultModelInfo.Result as CronJobStatusModel;

                if (_statusDetail != null)
                {
                    _statusDetail.ProcessDetails += $" Movie sync tool is {IsRunning} \r\n";

                    ResultModelInfo.Result = _statusDetail;
                }
            }
            else
                ResultModelInfo.Result = $" Movie sync tool is {IsRunning} \r\n";

            return await Task.FromResult(ResultModelInfo);
        }

        /// <summary>
        /// Sync isleminin baslatilmasini sagliyor
        /// </summary>
        /// <returns></returns>
        public async Task RunSyncTmdbMovieAsync()
        {
            IsRunning = true;

            CounterActiveSync += 1;

            try
            {
                var _data = await GetReadDataFromTmdbWebPage(false);

                if (_data == null || _data.results == null || _data.results.Count == 0)
                    return;

                using (var scope = ServiceProvider.CreateScope())
                {
                    var _unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();

                    // verileri kontrol edip güncelliyoruz
                    await MovieCheckAndUpdateDb(_unitOfWork, _data);
                }
            }
            catch (Exception _ex)
            {
                var _error = _ex;
            }
            finally
            {
                FinishThread();
            }
            
        }

        /// <summary>
        /// FinishThread methodu yardımı ile calisan thread bittiginde <see cref="CounterActiveSync"/> degerini
        /// 1 azaltip <see cref="IsRunning"/> degerini calisan hic thread kalmadiysa false duruma getiriyoruz
        /// </summary>
        private void FinishThread()
        {
            CounterActiveSync -= 1;

            if (CounterActiveSync == 0)
                IsRunning = false;
        }

        /// <summary>
        /// Tmdb web servisinden bilgi okumak icin kullanilan method
        /// </summary>
        /// <param name="syncAllData">Degeri true olarak atanirsa web servis uzerinde ki tum videolarin bilgilerini sayfa sayfa okuyacaktir</param>
        /// <returns></returns>
        private async Task<TmdbRequestDto> GetReadDataFromTmdbWebPage(bool syncAllData = true)
        {
            var _guid = Guid.NewGuid();
            var _resultStatus = new CronJobStatusModel();
            var _resultRequest = new TmdbRequestDto();
            var _activePage = 1;
            var _url = string.Format(
                            @"{0}
                                ?language=tr-TR
                                &sort_by=popularity.desc
                                &include_adult=false
                                &include_video=false
                                &page=[page]
                                &with_watch_monetization_types=flatrate"
                                , TmdbApiInfo.Discover)
                                    .Trim().Replace("\r\n", string.Empty).Replace("\t", string.Empty).Replace(" ", string.Empty);


            _resultStatus.Step = 1;
            _resultStatus.ProcessId = _guid;
            _resultStatus.ProcessDetails = "Servisten veri okuma islemi baslatildi";
            _resultStatus.TotalProcess = CounterActiveSync;

            ResultModelInfo.Result = _resultStatus;

            System.Threading.Thread.Sleep(1000);

            using (var _httpClient = new HttpClient())
            {
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TmdbApiInfo.V4Auth);

                _resultRequest = await GetDataFromTmdbWebPage(_httpClient, _url.Replace("[page]", _activePage.ToString()));

                //toplam sayfa sayisi 1 den buyuk ise isleme devam ediyoruz
                if (true == syncAllData && _resultRequest != null && _resultRequest.total_pages > 1)
                {
                    for (_activePage = 2; _activePage <= _resultRequest.total_pages; _activePage++)
                    {
                        _resultStatus.Step = _activePage;
                        _resultStatus.ProcessId = _guid;
                        _resultStatus.ProcessDetails = $"TotalPage: {_resultRequest.total_pages}    ActivePage: {_activePage}    ProcessDetail: Servisten veri okuma islemi devam ediyor";
                        ResultModelInfo.Result = _resultStatus;

                        var _data = await GetDataFromTmdbWebPage(_httpClient, _url.Replace("[page]", _activePage.ToString()));

                        if (_data == null)
                            break;

                        _resultRequest.results.AddRange(_data.results);
                    }
                }

                #region alternative

                //if (true == syncAllData && _result != null && _result.total_pages > 1)
                //{
                //    for (_activePage = 2; _activePage <= _result.total_pages; _activePage++)
                //    {
                //        using var _response = await _httpClient.GetAsync(_url.Replace("[page]", _activePage.ToString()));
                //        using var content = _response.Content;
                //        //get the json result from your api
                //        var _dataFromApi = await content.ReadAsStringAsync();

                //        if (_dataFromApi == null || string.IsNullOrWhiteSpace(_dataFromApi))
                //            break;

                //         var _deserialize = JsonSerializer.Deserialize<TmdbRequestDto>(_dataFromApi);

                //        if (_deserialize == null || _deserialize.results == null)
                //            break;

                //        _result.results.AddRange(_deserialize.results);
                //    }

                //}

                #endregion

            }

            return _resultRequest;
        }

        /// <summary>
        /// <seealso cref="GetReadDataFromTmdbWebPage(bool)"/> methodu icerisinde cagriliyor
        /// olup, servis uzerinden sayfa sayfa bilgi cekmek icin dongu icerisinde cagrilarak
        /// kullanilmaktadir
        /// </summary>
        /// <param name="httpClient"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        private async Task<TmdbRequestDto> GetDataFromTmdbWebPage(HttpClient httpClient, string url)
        {
            using var _response = await httpClient.GetAsync(url);
            using var content = _response.Content;
            //get the json result from your api
            var _dataFromApi = await content.ReadAsStringAsync();

            if (!string.IsNullOrWhiteSpace(_dataFromApi))
                return JsonSerializer.Deserialize<TmdbRequestDto>(_dataFromApi);

            return null;
        }

        /// <summary>
        /// Film bilgilerini kontrol edip veritabanina yazan method
        /// Kontrol için servisten gelen verinin id bilgisi ile db uzerindeki kaydin syncId bilgisi karsilastirilip
        /// ilgili kaydin diger bilgileri guncelleniyor
        /// </summary>
        /// <param name="movieData"></param>
        /// <returns></returns>
        private async Task<bool> MovieCheckAndUpdateDb(IUnitOfWork unitOfWork,TmdbRequestDto movieData)
        {
            var _resultStatus = new CronJobStatusModel();
            _resultStatus.Step = 1;
            _resultStatus.ProcessDetails = $"Db ile senkronizasyon işlemi başlatıldı";
            ResultModelInfo.Result = _resultStatus;

            var _counter = movieData?.results?.Count ?? 1;
            var _step = 1;

            // ilk adim veritabaninda olan filmlerin listesini cikarmak
            foreach (var movie in movieData.results)
            {
                _step += 1;
                _resultStatus.Step = _step;
                _resultStatus.ProcessDetails = $"TotalRecord: {_counter}    Step: {_step}    SyncId: {movie.id}    Nolu veri senkronize ediliyor";
                ResultModelInfo.Result = _resultStatus;

                var _movieDataByDB = await unitOfWork.Movies.GetWithMovieBySyncIdAsync(movie.id);

                if (_movieDataByDB == null)
                {
                    _movieDataByDB = new Movie();
                    _movieDataByDB.Overview = movie.overview;
                    _movieDataByDB.Title = movie.title;
                    _movieDataByDB.OriginalLanguage = movie.original_language;
                    _movieDataByDB.OriginalTitle = movie.original_title;
                    _movieDataByDB.Popularity = movie.popularity;
                    _movieDataByDB.PosterPath = movie.poster_path;
                    _movieDataByDB.ReleaseDate = movie.release_date;
                    _movieDataByDB.BackdropPath = movie.backdrop_path;
                    _movieDataByDB.Video = movie.video;

                    _movieDataByDB.UserId = -1;
                    _movieDataByDB.SyncId = movie.id;

                    await unitOfWork.Movies.AddAsync(_movieDataByDB);
                }
                else
                {
                    _movieDataByDB.Overview = movie.overview;
                    _movieDataByDB.Title = movie.title;
                    _movieDataByDB.OriginalLanguage = movie.original_language;
                    _movieDataByDB.OriginalTitle = movie.original_title;
                    _movieDataByDB.Popularity = movie.popularity;
                    _movieDataByDB.PosterPath = movie.poster_path;
                    _movieDataByDB.ReleaseDate = movie.release_date;
                    _movieDataByDB.BackdropPath = movie.backdrop_path;
                    _movieDataByDB.Video = movie.video;

                    _movieDataByDB.UserId = -1;
                }
            }

            _resultStatus.ProcessDetails = $"Veri senkronizasyonu tamamlaniyor";
            ResultModelInfo.Result = _resultStatus;

            // degisiklikleri uyguluyoruz
            await unitOfWork.CommitAsync(true);

            return true;
        }

        #region Demo_RunSyncTmdbMovieAsync
        //public async Task RunSyncTmdbMovieAsync()
        //{
        //    IsRunning = true;

        //    CounterActiveSync += 1;

        //    await Task.Run(
        //       () =>
        //       {
        //           var _guid = Guid.NewGuid();
        //           var _result = new CronJobStatusModel();

        //           for (int _counter = 0; _counter < 50; _counter++)
        //           {
        //               System.Threading.Thread.Sleep(800);

        //               _result.Step = _counter + 1;
        //               _result.ProcessId = _guid;
        //               _result.ProcessDetails = "Islem devem ediyor";
        //               _result.TotalProcess = CounterActiveSync;

        //               ResultModelInfo.Result = _result;
        //           }

        //           CounterActiveSync -= 1;

        //           if (CounterActiveSync == 0)
        //               IsRunning = false;
        //       }
        //   );
        //}
        #endregion

    }
}
