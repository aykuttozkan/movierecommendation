﻿using MovieRecommendation.Core.Tools;
using MovieRecommendation.Domain;
using MovieRecommendation.Domain.Dtos;
using MovieRecommendation.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRecommendation.Bll.Concrete
{
    public class VoteManager : IVoteService
    {
        private readonly IUnitOfWork _UnitOfWork;

        public VoteManager(IUnitOfWork unitOfWork)
        {
            _UnitOfWork = unitOfWork;
        }

        public async Task<ResultModel> AddCommentAndRate(CommentAndVoteDto commentAndVote, int userID)
        {
            var _result = new ResultModel();

            var _movie = await _UnitOfWork.Movies.GetByIdAsync(commentAndVote.MovieId);

            if(_movie == null)
            {
                _result.Error = true;
                _result.Result = "Geçersiz film bilgisi";

                return _result;
            }

            var _commentCheckFromDb = await _UnitOfWork
                                                .Votes
                                                    .FirstOrDefaultAsync(
                                                        x => x.UserId == userID 
                                                        && x.MovieId == commentAndVote.MovieId
                                                     );

            if(_commentCheckFromDb != null && _commentCheckFromDb.Id > 0)
            {
                _commentCheckFromDb.VoteNote = commentAndVote.Comment;
                _commentCheckFromDb.VoteValue = commentAndVote.Rating;

                _result.Result = "Mevcut yorumunuz ve puanınız güncellenmiştir.";
            }
            else
            {
                await _UnitOfWork.Votes.AddCommentAndVote(commentAndVote, userID);

                _result.Result = "Yorumunuz ve puanınız kaydedilmiştir.";
            }

            await _UnitOfWork.CommitAsync(true);

            return _result;
        }
    }
}
