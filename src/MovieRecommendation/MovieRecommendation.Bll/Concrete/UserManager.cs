﻿using MovieRecommendation.Core.Extentions;
using MovieRecommendation.Domain;
using MovieRecommendation.Domain.Dtos;
using MovieRecommendation.Domain.Entities;
using MovieRecommendation.Domain.Interfaces;
using System.Threading.Tasks;

namespace MovieRecommendation.Bll.Concrete
{
    public class UserManager : IUserService
    {
        private readonly IUnitOfWork _UnitOfWork;

        public UserManager(IUnitOfWork unitOfWork)
        {
            _UnitOfWork = unitOfWork;
        }

        /// <summary>
        /// UserId bilgisi ile eslesen veritabani kullanicisina
        /// ait bilgilerin geri alinmasini saglar
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<User> GetUserAsync(int userId)
        {
            return await _UnitOfWork.Users.FirstOrDefaultAsync(x => x.Id == userId);
        }

        /// <summary>
        /// <seealso cref="LoginDto"/> nesnesi ile gonderilen bilgilere gore
        /// veritabani uzerinde kontrol yapip bilgiler dogru ise geriye
        /// <seealso cref="User"/> nesnesini dondurur
        /// </summary>
        /// <param name="loginDto"></param>
        /// <returns></returns>
        public async Task<User> LoginAsync(LoginDto loginDto)
        {
            if (loginDto == null)
                return null;

            if (string.IsNullOrWhiteSpace(loginDto.UserName) || string.IsNullOrWhiteSpace(loginDto.Password))
                return null;

            loginDto.Password = loginDto.Password.ToSha512();

            return await _UnitOfWork.Users.LoginAsync(loginDto);
        }
    }
}
