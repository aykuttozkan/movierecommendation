﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MovieRecommendation.Dal;
using MovieRecommendation.Dal.Concrete;
using MovieRecommendation.Dal.Context;
using MovieRecommendation.Domain;
using MovieRecommendation.Domain.Abstract;

namespace MovieRecommendation.Bll.ServiceCollectionExtentions
{
    public static class ServiceCollectionExtentions
    {
        public static IServiceCollection AddDataServiceForSqlLite(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<MovieDbContext>(options =>
                options.UseSqlite(
                        configuration.GetConnectionString("DefaultConnectionForSqLite")
                        , b => b.MigrationsAssembly("MovieRecommendation.Dal")
                    )
            );

            services.AddScoped<DbContext, MovieDbContext>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IMovieRepository, MovieRepository>();
            services.AddScoped<IVoteRepository, VoteRepository>();

            return services;
        }
    }
}
