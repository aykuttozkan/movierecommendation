﻿using Hangfire;
using Microsoft.Extensions.Configuration;
using MovieRecommendation.Domain.Abstract;
using MovieRecommendation.Domain.Interfaces;
using System;

namespace MovieRecommendation.WebApi.Hangfire
{
    public class HangfireJobs : IHangfireJobs
    {
        private readonly IConfiguration Configuration;
        private readonly ICronJobs CronJobs;

        public HangfireJobs(IConfiguration configuration, ICronJobs cronJobs)
        {
            Configuration = configuration;
            CronJobs = cronJobs;

            //https://crontab.guru/
            //RecurringJob.AddOrUpdate("JobId", () => ProcessRecurringJob(), "*/1 * * * *", queue: "default");

            Init();
        }

        /// <summary>
        /// Hangfire zamanlanmiş görevlerin tanımlandığı method
        /// </summary>
        private void Init()
        {
            // default queue altında jobId id bilgisi ile 59 dk ara ile çalışan bir job tanımlıyoruz
            RecurringJob.AddOrUpdate("JobId", () => CronJobs.RunSyncTmdbMovieAsync(), "*/59 * * * *", queue: "default");
        }

        public void ProcessRecurringJob()
        {
            System.Diagnostics.Debug.WriteLine($"Process Time: {DateTime.Now}    Process Detail: CronJob Çalıştı");
        }
    }
}
