﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MovieRecommendation.Domain.Dtos;
using MovieRecommendation.Domain.Interfaces;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using System;
using Microsoft.AspNetCore.Authorization;
using System.Collections;
using MovieRecommendation.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using MovieRecommendation.Core.Tools;

namespace MovieRecommendation.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly ILogger<AccountController> Logger;
        private readonly IUserService UserService;
        private readonly JwtConfigModel _JwtConfigModel;

        public AccountController(ILogger<AccountController> logger, IUserService userService, IOptions<JwtConfigModel> options)
        {
            Logger = logger;
            UserService = userService;

            _JwtConfigModel = options.Value;
        }


        /// <summary>
        /// Id bilgisine ait olan video bilgisini dondurur
        /// </summary>
        /// <param name="id">Film id bilgisi</param>
        /// <returns></returns>
        [Route("login")]
        [HttpPost]
        public async Task<ResultModel> Login([FromBody] LoginDto loginModel)
        {
            var _user = await UserService.LoginAsync(loginModel);

            if (_user == null)
                return null;

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_JwtConfigModel.Secret));
            var signinCredential = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);
            var jwt = new JwtSecurityToken(
                    issuer: _JwtConfigModel.Issuer,
                    audience: _JwtConfigModel.Audience,
                    expires: DateTime.Now.AddMinutes(_JwtConfigModel.ExpMinute),
                    notBefore: DateTime.Now,
                    claims: SetClaims(_user, new List<OperationClaim>()
                    {
                        new OperationClaim{ Id = 1, Name = "Admin" },
                        new OperationClaim{ Id = 2, Name = "User" },
                        new OperationClaim{ Id = 2, Name = "Senior" }
                    }),
                    signingCredentials: signinCredential
                );

            var jwtHandle = new JwtSecurityTokenHandler();
            var token = jwtHandle.WriteToken(jwt);

            _user.Token = token;
            _user.Password = null;

            return await Task.FromResult(new ResultModel { Result = _user });
        }

        /// <summary>
        /// Kullanıcıya ait user bilgilerini döndürür. Lakin öncelikle ilgili kullanıcının login olması ve token bilgisinin bulunması gerekmektedir.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Manager, Senior")]
        [Route("UserInfo")]
        [HttpGet]
        public async Task<ResultModel> GetUserInfo()
        {
            //var _userNameAndSurname = User.FindFirst(ClaimTypes.Name)?.Value;
            var _userID = (User.FindFirst(ClaimTypes.NameIdentifier) != null) 
                            ? Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value) 
                            : -1;

            if (_userID <= 0)
                return await Task.FromResult(new ResultModel { Result = "Hatalı veya geçersiz kullanıcı bilgisi", StatusCode = StatusCodes.Status400BadRequest });

            var _user = await UserService.GetUserAsync(_userID);
            var _result = new UserResponseDto();

            if (_user != null)
                _result = _user.ToResponseDto();

            return await Task.FromResult(new ResultModel { Result = _result, StatusCode = StatusCodes.Status200OK });
        }

        /// <summary>
        /// <seealso cref="Login(LoginDto)"/> methodu içerisinde çağrılıyor olup, kullanıcı yetki ve rollerin
        /// claim içerisine atanmasını sağlıyor
        /// </summary>
        /// <param name="user"></param>
        /// <param name="operationClaims"></param>
        /// <returns></returns>
        private IEnumerable<Claim> SetClaims(User user, List<OperationClaim> operationClaims)
        {
            var claims = new List<Claim>();
            claims.Add(new Claim(JwtRegisteredClaimNames.Email, user.Email));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
            claims.Add(new Claim(ClaimTypes.Name, $"{ user.Name} { user.Surname}"));
            claims.Add(new Claim("Status", "true"));

            operationClaims.Select(x => x.Name).ToList().ForEach(role => claims.Add(new Claim(ClaimTypes.Role, role)));

            return claims;
        }
    }
}
