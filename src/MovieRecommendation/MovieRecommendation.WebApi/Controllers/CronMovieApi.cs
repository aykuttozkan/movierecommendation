﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MovieRecommendation.Core.Tools;
using MovieRecommendation.Domain.Abstract;
using MovieRecommendation.Domain.Dtos;
using MovieRecommendation.Domain.Interfaces;
using System.Threading.Tasks;

namespace MovieRecommendation.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CronMovieApi : ControllerBase
    {
        private readonly IConfiguration Configuration;
        private readonly ITmdbApiInfo TmdbApiInfo;
        private readonly ICronJobs CronJobs;

        public CronMovieApi(IConfiguration configuration, ITmdbApiInfo tmdbApiInfo, ICronJobs cronJobs)
        {
            Configuration = configuration;
            TmdbApiInfo = tmdbApiInfo;
            CronJobs = cronJobs;
            //CronJobs = new CronJobs(TmdbApiInfo);
        }

        /// <summary>
        /// Manuel olarak sync işleminin başlatılmasını sağlar
        /// </summary>
        /// <returns></returns>
        [Route("runSync")]
        [HttpGet]
        public async Task<ResultModel> GetRunSync()
        {
            CronJobs.RunSyncTmdbMovieAsync();
            ThreadTools.Delay(250);

            return await CronJobs.GetSyncStatusAsync();
            //return await Task.FromResult(new ResultModel { Result = TmdbApiInfo });
        }

        /// <summary>
        /// Arka planda başlatılan sync bilgisini döndürür
        /// </summary>
        /// <returns></returns>
        [Route("getSyncStatus")]
        [HttpGet]
        public async Task<ResultModel> GetSyncStatus()
        {
            //return await Task.FromResult(new ResultModel { Result = TmdbApiInfo });
            return await CronJobs.GetSyncStatusAsync();
        }
    }
}
