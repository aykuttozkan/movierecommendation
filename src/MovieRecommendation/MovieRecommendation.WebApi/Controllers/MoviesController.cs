﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MovieRecommendation.Domain.Dtos;
using MovieRecommendation.Domain.Interfaces;
using MovieRecommendation.WebApi.Filters;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MovieRecommendation.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly ILogger<MoviesController> Logger;
        private readonly IMovieService MovieService;
        private readonly IVoteService VoteService;

        public MoviesController(ILogger<MoviesController> logger, IMovieService movieService, IVoteService voteService)
        {
            Logger = logger;
            MovieService = movieService;
            VoteService = voteService;
        }

        /// <summary>
        /// Id bilgisine ait olan video bilgisini dondurur
        /// </summary>
        /// <param name="id">Film id bilgisi</param>
        /// <returns></returns>
        [Authorize]
        [Route("moviebyid")]
        [HttpGet]
        public async Task<ResultModel> GetMovie(int id)
        {
            var _userID = (User.FindFirst(ClaimTypes.NameIdentifier) != null)
                            ? Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value)
                            : -1;

            var _movie = await MovieService.GetMovieById(id, _userID);

            return await Task.FromResult(new ResultModel { Result = _movie });
        }


        /// <summary>
        /// Film bilgilerini sayfalama yontemi ile almak icin kullanilan action method
        /// </summary>
        /// <param name="paginationDto">
        /// Sayfada gosterilecek veri sayısı ve gidilmek istenen sayfa bilgisinin bulundugu dto model nesnesi
        /// </param>
        /// <returns></returns>
        [Authorize]
        [Route("getallmovie")]
        [HttpPost]
        public async Task<ResultModel> GetMovieWithPagination([FromBody] PaginationDto paginationDto)
        {
            var _result = new ResultModel();

            var _movie = await MovieService.GetAllMoviesByPageFilter(paginationDto);

            if (_movie == null)
                return new ResultModel { 
                    StatusCode= StatusCodes.Status403Forbidden
                    , Error = true
                    , ErrorResult = "Göndermiş olduğunuz bilgiler ile eşleşen kayıt bulunamamıştır."  
                };

            _result.Result = _movie;
            _result.StatusCode = StatusCodes.Status200OK;

            return await Task.FromResult(_result);
        }

        [Authorize]
        [ValidateModel]
        [Route("addcommentandvote")]
        [HttpPost]
        public async Task<ResultModel> PostAddCommitAndVote(CommentAndVoteDto commentAndVote)
        {
            var _userID = (User.FindFirst(ClaimTypes.NameIdentifier) != null)
                            ? Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value)
                            : -1;

            var _data = await VoteService.AddCommentAndRate(commentAndVote, _userID);

            if (_data.Error)
                _data.StatusCode = StatusCodes.Status400BadRequest;
            else
                _data.StatusCode = StatusCodes.Status200OK;

            return await Task.FromResult(_data);
        }

        [Authorize]
        [ValidateModel]
        [Route("movierecommend")]
        [HttpPost]
        public async Task<ResultModel> PostMovieRecommend(int movieID, string toEmail)
        {
            var _userID = (User.FindFirst(ClaimTypes.NameIdentifier) != null)
                            ? Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value)
                            : -1;

            var _sendMail = await MovieService.Recommend(movieID, _userID, toEmail);

            var _result = new ResultModel();

            _result.Result = (_sendMail == true) 
                                ? "Film tavsiye mailiniz başarılı bir şekilde gönderilmiştir." 
                                : "Mail gönderim işlemi sırasında bir hata meydana geldi. Lütfen daha sonra tekrar deneyiniz.";
            
            _result.Error = (_sendMail == false) 
                                ? true 
                                : false;

            _result.StatusCode = (_sendMail == true) 
                                    ? StatusCodes.Status200OK 
                                    : StatusCodes.Status400BadRequest;

            return await Task.FromResult(_result);
        }
    }
}
