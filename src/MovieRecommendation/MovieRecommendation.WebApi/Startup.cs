using Hangfire;
using Hangfire.LiteDB;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using MovieRecommendation.Bll.Concrete;
using MovieRecommendation.Bll.ServiceCollectionExtentions;
using MovieRecommendation.Core.Tools;
using MovieRecommendation.Domain.Abstract;
using MovieRecommendation.Domain.Dtos;
using MovieRecommendation.Domain.Interfaces;
using MovieRecommendation.WebApi.Hangfire;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Text;

namespace MovieRecommendation.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDataServiceForSqlLite(Configuration);

            var _tmdbApiInfo = new TmdbApiInfo();
            var _confMailServer = new ConfMailServer();

            // appsettings.json data to model
            Configuration.GetSection("TmdbApiInfo").Bind(_tmdbApiInfo);
            Configuration.GetSection("MailServer").Bind(_confMailServer);
            services.Configure<JwtConfigModel>(Configuration.GetSection("JwtConfig"));

            services.AddSingleton<ITmdbApiInfo>(x=> _tmdbApiInfo);
            services.AddSingleton<IConfMailServer>(x=> _confMailServer);
            services.AddSingleton<ICronJobs, CronJobManager>();
            services.AddSingleton<IHangfireJobs, HangfireJobs>();
            services.AddScoped<IMovieService, MovieManager>();
            services.AddScoped<IUserService, UserManager>();
            services.AddScoped<IVoteService, VoteManager>();
            services.AddScoped<IMailTools, MailTools>();

            //services.AddSingleton<ICronJobs>(x=> new CronJobs(_tmdbApiInfo));

            #region Jwt
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
           .AddJwtBearer(jwt => {
               var key = Encoding.ASCII.GetBytes(Configuration["JwtConfig:Secret"]);

               jwt.SaveToken = true;
               jwt.TokenValidationParameters = new TokenValidationParameters
               {
                   ValidateIssuerSigningKey = true, // this will validate the 3rd part of the jwt token using the secret that we added in the appsettings and verify we have generated the jwt token
                    IssuerSigningKey = new SymmetricSecurityKey(key), // Add the secret key to our Jwt encryption
                    ValidateIssuer = true,
                   ValidateAudience = true,
                   ValidateLifetime = true,
                   RequireExpirationTime = false,
                   ClockSkew = TimeSpan.Zero,
                   ValidIssuer = Configuration["JwtConfig:Issuer"],
                   ValidAudience = Configuration["JwtConfig:Audience"]
               };
           });
            #endregion

            #region Hangfire
            string hangfireConnectionString = Configuration.GetConnectionString("HangfireConnectionForSqlite");
            var _liteDb = new LiteDbStorage(hangfireConnectionString);
            // Add Hangfire services.
            services
             .AddHangfire(config => config
             .UseLiteDbStorage(hangfireConnectionString, new LiteDbStorageOptions
             {
                 InvisibilityTimeout = TimeSpan.FromDays(10)
             }));

            JobStorage.Current = _liteDb;

            services.AddHangfireServer();
            #endregion

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "MovieRecommendation.WebApi", Version = "v1" });

                c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                {
                    Description = "Standard Authorization header using the Bearer scheme. Example: \"bearer {token}\"",
                    In = ParameterLocation.Header,
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });

                // https://mattfrear.com/2018/07/21/add-an-authorization-header-to-your-swagger-ui-with-swashbuckle-revisited/
                c.OperationFilter<SecurityRequirementsOperationFilter>();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "MovieRecommendation.WebApi v1"));
            }

            // global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            // HangFireJobs sinifini tetikliyoruz
            app.ApplicationServices.GetService<IHangfireJobs>();

            app.UseHangfireDashboard();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
