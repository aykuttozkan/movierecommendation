﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MovieRecommendation.Core.Extentions
{
    public static class StringExtentions
    {
        public static string ToSha512(this string data)
        {
            if (string.IsNullOrWhiteSpace(data))
                return null;

            var _result = string.Empty;

            using (SHA512 sha512 = new SHA512Managed())
            {
                var _sha512EncodingData = sha512.ComputeHash(Encoding.UTF8.GetBytes(data));
                
                StringBuilder _builder = new StringBuilder();

                for (int counter = 0; counter < _sha512EncodingData.Length; counter++)
                {
                    _builder.Append(_sha512EncodingData[counter].ToString("x2"));
                }

                _result = _builder.ToString();
            }

            return _result;
        }
    }
}
