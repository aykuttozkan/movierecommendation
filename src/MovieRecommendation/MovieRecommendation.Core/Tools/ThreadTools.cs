﻿namespace MovieRecommendation.Core.Tools
{
    public static class ThreadTools
    {
        public static void Delay(int delayMilliSecond)
        {
            System.Threading.Thread.Sleep(delayMilliSecond);
        }
    }
}
