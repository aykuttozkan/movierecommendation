﻿using MovieRecommendation.Domain.Abstract;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace MovieRecommendation.Core.Tools
{
    public class MailTools : IMailTools
    {
        private static IConfMailServer _ConfMailServer;

        public MailTools(IConfMailServer confMailServer)
        {
            _ConfMailServer = confMailServer;
        }

        public async Task<bool> SendEmailAsync(string emailAddress, string subject, string message)
        {
            using (var client = new SmtpClient(_ConfMailServer.Host))
            {
                client.Port = _ConfMailServer.SmtpPort;
                client.EnableSsl = _ConfMailServer.EnableSSL;
                
                if(!string.IsNullOrWhiteSpace(_ConfMailServer.CredentialUser) && !string.IsNullOrWhiteSpace(_ConfMailServer.CredentialPass))
                {
                    client.Credentials = new NetworkCredential(_ConfMailServer.CredentialUser, _ConfMailServer.CredentialPass);
                }

                var mailMessage = new MailMessage("no-reply@movierecommandation.io", emailAddress, subject, message)
                {
                    IsBodyHtml = true
                };

                await client.SendMailAsync(mailMessage);
            }

            return true;
        }
    }
}
