﻿using MovieRecommendation.Domain.Abstract;
using MovieRecommendation.Domain.Dtos;
using MovieRecommendation.Domain.Entities;
using System.Linq;

namespace MovieRecommendation.Core.Tools
{
    public static class EntityModelTools
    {
        public static ResultMovieDto ToDto(this Movie movie)
            => new()
            {
                SyncId = movie?.SyncId ?? -1,
                Id = movie?.Id ?? -1,
                Title = movie?.Title ?? string.Empty,
                Overview = movie?.Overview ?? string.Empty,
                BackdropPath = movie?.BackdropPath ?? string.Empty,
                OriginalLanguage = movie?.OriginalLanguage ?? string.Empty,
                OriginalTitle = movie?.OriginalTitle ?? string.Empty,
                Popularity = movie?.Popularity ?? 0,
                PosterPath = movie?.PosterPath ?? string.Empty,
                ReleaseDate = movie?.ReleaseDate ?? string.Empty,
                Video = movie?.Video ?? false,
                VoteCount = movie?.Votes?.Count ?? -1,
                VoteAverage = (movie != null) 
                                ? movie.Votes != null && movie.Votes.Count > 0 ?  movie.Votes.Average(p => p.VoteValue) : 0
                                : 0
                
            };

        public static ResultMovieDto ToDto(this Movie movie, ITmdbApiInfo tmdbApiInfo)
            => new()
            {
                SyncId = movie?.SyncId ?? -1,
                Id = movie?.Id ?? -1,
                Title = movie?.Title ?? string.Empty,
                Overview = movie?.Overview ?? string.Empty,
                BackdropPath = !string.IsNullOrWhiteSpace(movie?.BackdropPath) ? string.Concat(tmdbApiInfo.ImageUrl, movie.BackdropPath) : string.Empty,
                OriginalLanguage = movie?.OriginalLanguage ?? string.Empty,
                OriginalTitle = movie?.OriginalTitle ?? string.Empty,
                Popularity = movie?.Popularity ?? 0,
                PosterPath = (!string.IsNullOrWhiteSpace(movie?.PosterPath)) ? string.Concat(tmdbApiInfo.ImageUrl, movie.PosterPath) : string.Empty,
                ReleaseDate = movie?.ReleaseDate ?? string.Empty,
                Video = movie?.Video ?? false,
                VoteCount = movie?.Votes?.Count ?? -1,
                VoteAverage = (movie != null)
                                ? movie.Votes != null && movie.Votes.Count > 0 ? movie.Votes.Average(p => p.VoteValue) : 0
                                : 0

            };

        public static UserResponseDto ToResponseDto(this User user)
            => new()
            {
                UserId = user.Id,
                Guid = user.Guid,
                UserName = user.UserName,
                Email = user.Email,
                Name = user.Name,
                Surname = user.Surname
            };

        public static Vote ToEntity(this CommentAndVoteDto commentAndVoteDto)
            => new()
            {
                MovieId = commentAndVoteDto.MovieId,
                VoteNote = commentAndVoteDto.Comment,
                VoteValue = commentAndVoteDto.Rating
            };
    }
}
