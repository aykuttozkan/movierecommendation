﻿using Microsoft.EntityFrameworkCore.Migrations;
using MovieRecommendation.Core.Extentions;
using System;

namespace MovieRecommendation.Dal.Migrations
{
    public partial class _02_addDefaultUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData("Users"
                , new string[] { "Guid", "Name", "Surname", "UserName", "Email", "Password" }
                , new string[] { Guid.NewGuid().ToString(), "Demo User Name", "Surname", "demouser", "demo@demo.com", "123456".ToSha512() });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
