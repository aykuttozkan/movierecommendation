﻿using Microsoft.EntityFrameworkCore;
using MovieRecommendation.Domain.Entities;

namespace MovieRecommendation.Dal.Context
{
    public class MovieDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }

        public DbSet<Vote> Votes { get; set; }

        public DbSet<User> Users { get; set; }

        public MovieDbContext(DbContextOptions<MovieDbContext> options) 
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);

            //base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Movie>()
                .Property(b => b.IsActive)
                .HasDefaultValue(true);

            modelBuilder.Entity<Movie>()
                .Property(b => b.IsDelete)
                .HasDefaultValue(false);
        }
    }
}
