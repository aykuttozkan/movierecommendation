﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using MovieRecommendation.Dal.Concrete;
using MovieRecommendation.Domain;
using MovieRecommendation.Domain.Abstract;
using System.Threading.Tasks;

namespace MovieRecommendation.Dal
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _Context;
        private IMovieRepository _MovieRepository;
        private IUserRepository _UserRepository;
        private IVoteRepository _VoteRepository;

        IDbContextTransaction Transaction = null;

        public UnitOfWork(DbContext dbContext)
        {
            _Context = dbContext;

            Transaction = _Context.Database.BeginTransaction();
        }

        public IMovieRepository Movies => _MovieRepository = _MovieRepository ?? new MovieRepository(_Context);

        public IUserRepository Users => _UserRepository = _UserRepository ?? new UserRepository(_Context);

        public IVoteRepository Votes => _VoteRepository = _VoteRepository ?? new VoteRepository(_Context);

        public async Task<int> CommitAsync(bool state = true)
        {
            var _saveChangesAsync = await _Context.SaveChangesAsync();

            if (state)
                Transaction.Commit();
            else
                Transaction.Rollback();

            return _saveChangesAsync;
        }

        public void Dispose()
        {
            _Context.Dispose();
        }
    }
}
