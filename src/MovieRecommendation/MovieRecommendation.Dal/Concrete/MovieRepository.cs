﻿using Microsoft.EntityFrameworkCore;
using MovieRecommendation.Dal.Context;
using MovieRecommendation.Domain.Abstract;
using MovieRecommendation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRecommendation.Dal.Concrete
{
    public class MovieRepository : Repository<Movie>, IMovieRepository
    {
        private MovieDbContext SQLiteDbContext
        {
            get { return Context as MovieDbContext; }
        }

        public MovieRepository(DbContext dbContext)
            : base(dbContext)
        {

        }

        public async Task<IEnumerable<Movie>> GetAllWithMovieAsync()
        {
            return await SQLiteDbContext.Movies
                .ToListAsync();
        }

        public async Task<IEnumerable<Movie>> GetAllWithMovieByMovieIdAsync(int movieId)
        {
            return await SQLiteDbContext.Movies
                .Where(x => x.Id == movieId)
                .ToListAsync();
        }

        public async Task<Movie> GetWithMovieByIdAsync(int id)
        {
            return await SQLiteDbContext.Movies
                .Include(i => i.Votes)
                //.Include(i => i.Vote)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<Movie> GetWithMovieBySyncIdAsync(int syncId)
        {
            return await SQLiteDbContext.Movies
                 .Include(i => i.Votes)
                 .FirstOrDefaultAsync(x => x.SyncId == syncId);
        }
    }
}
