﻿using Microsoft.EntityFrameworkCore;
using MovieRecommendation.Dal.Context;
using MovieRecommendation.Domain.Abstract;
using MovieRecommendation.Domain.Dtos;
using MovieRecommendation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MovieRecommendation.Dal.Concrete
{
    public class VoteRepository : Repository<Vote>, IVoteRepository
    {
        private MovieDbContext _DbContext
        {
            get { return Context as MovieDbContext; }
        }

        public VoteRepository(DbContext dbContext)
           : base(dbContext)
        {

        }

        public Task AddAsync(Vote entity)
        {
            throw new NotImplementedException();
        }

        public async Task AddCommentAndVote(CommentAndVoteDto commentAndVote, int userID)
        {
            await _DbContext.AddAsync(
                            new Vote
                            {
                                MovieId = commentAndVote.MovieId,
                                UserId = userID,
                                VoteNote = commentAndVote.Comment,
                                VoteValue = commentAndVote.Rating
                            }
                           );
        }
    }
}
