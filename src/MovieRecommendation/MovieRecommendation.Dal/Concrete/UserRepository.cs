﻿using Microsoft.EntityFrameworkCore;
using MovieRecommendation.Dal.Context;
using MovieRecommendation.Domain.Abstract;
using MovieRecommendation.Domain.Dtos;
using MovieRecommendation.Domain.Entities;
using System.Threading.Tasks;

namespace MovieRecommendation.Dal.Concrete
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private MovieDbContext _DbContext
        {
            get { return Context as MovieDbContext; }
        }

        public UserRepository(DbContext dbContext)
            : base(dbContext)
        {            
        }

        public async Task<User> LoginAsync(LoginDto loginDto)
        {
            return await _DbContext.Users
                            .FirstOrDefaultAsync(x => x.UserName == loginDto.UserName && x.Password == loginDto.Password);
        }
    }
}
