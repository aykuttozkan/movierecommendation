using MovieRecommendation.Core.Extentions;
using Xunit;
using Xunit.Abstractions;

namespace MovieRecommendation.XUnitTest
{
    public class StringExtentionsTest
    {

        /// <summary>
        /// https://xunit.net/docs/capturing-output
        /// </summary>
        private readonly ITestOutputHelper Output;

        public StringExtentionsTest(ITestOutputHelper output)
        {
            Output = output;
        }

        [Theory]
        [InlineData("123456")]
        public void ToSha512FromString(string data)
        {
            Assert.False(string.IsNullOrWhiteSpace(data), "Data parametresi null veya bo� ge�ilemez");

            var _sha512EncodingData = data.ToSha512();

            Assert.Equal("ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413", _sha512EncodingData);

            Output.WriteLine("�ifreler E�le�mektedir.");
        }
    }
}
