﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MovieRecommendation.Bll.Concrete;
using MovieRecommendation.Core.Tools;
using MovieRecommendation.Domain.Abstract;
using MovieRecommendation.Domain.Dtos;
using MovieRecommendation.Domain.Interfaces;
using MovieRecommendation.WebApi.Hangfire;
using Xunit;
using Xunit.Abstractions;

namespace MovieRecommendation.XUnitTest
{
    public class MailToolsTest : IClassFixture<DependencySetupFixture>
    {
        // <summary>
        /// https://xunit.net/docs/capturing-output
        /// </summary>
        private readonly ITestOutputHelper Output;
        private ServiceProvider _serviceProvide;

        public MailToolsTest(ITestOutputHelper output, DependencySetupFixture fixture)
        {
            Output = output;
            _serviceProvide = fixture.ServiceProvider;
        }

        [Fact]
        public async void SendMail()
        {
            //var _result = await MailTools.SendEmailAsync("cikit70119@whipjoy.com", "Demo Mail", "Demo mail içeriği");

            //Assert.True(_result, "Mail gönderim işlemi başarılıdır");

            //Assert.False(string.IsNullOrWhiteSpace(data), "Data parametresi null veya boş geçilemez");
        }
    }

    public class DependencySetupFixture
    {
        public DependencySetupFixture()
        {
            //var configuration = new ConfigurationBuilder();

            var _serviceCollection = new ServiceCollection();
            var _tmdbApiInfo = new TmdbApiInfo();
            var _confMailServer = new ConfMailServer();

            // appsettings.json data to model
            Configuration.GetSection("TmdbApiInfo").Bind(_tmdbApiInfo);
            Configuration.GetSection("MailServer").Bind(_confMailServer);
            _serviceCollection.Configure<JwtConfigModel>(Configuration.GetSection("JwtConfig"));

            _serviceCollection.AddSingleton<ITmdbApiInfo>(x => _tmdbApiInfo);
            _serviceCollection.AddSingleton<IConfMailServer>(x => _confMailServer);
            _serviceCollection.AddSingleton<ICronJobs, CronJobManager>();
            _serviceCollection.AddSingleton<IHangfireJobs, HangfireJobs>();
            _serviceCollection.AddScoped<IMovieService, MovieManager>();
            _serviceCollection.AddScoped<IUserService, UserManager>();
            _serviceCollection.AddScoped<IVoteService, VoteManager>();
            _serviceCollection.AddScoped<IMailTools, MailTools>();
        }

        public IConfiguration Configuration
        {
            get
            {
                if (configuration == null)
                {
                    var builder = new ConfigurationBuilder().AddJsonFile($"appsettings.json", optional: false);
                    configuration = builder.Build();
                }

                return configuration;
            }
        }

        public ServiceProvider ServiceProvider { get; private set; }

        private IConfiguration configuration;
    }
}
