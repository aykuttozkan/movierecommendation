﻿using System;
using System.Threading.Tasks;
using MovieRecommendation.Domain.Abstract;

namespace MovieRecommendation.Domain
{
    public interface IUnitOfWork : IDisposable
    {
        IMovieRepository Movies { get; }
        IUserRepository Users { get; }
        IVoteRepository Votes { get; }

        Task<int> CommitAsync(bool state = true);
    }
}
