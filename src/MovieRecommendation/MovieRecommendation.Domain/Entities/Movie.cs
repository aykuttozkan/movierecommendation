﻿using MovieRecommendation.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieRecommendation.Domain.Entities
{
    public class Movie : BaseEntity<int>
    {

        public Movie()
        {
            Votes = new Collection<Vote>();
        }

        public int SyncId { get; set; }

        public string Title { get; set; }
        
        public string Overview { get; set; }

        public int UserId { get; set; }

        public string OriginalLanguage { get; set; }

        public string OriginalTitle { get; set; }

        public Nullable<float> Popularity { get; set; }

        public string PosterPath { get; set; }

        public string ReleaseDate { get; set; }

        public Nullable<bool> Video { get; set; }

        public string BackdropPath { get; set; }

        public Nullable<bool> IsActive { get; set; }
        
        public Nullable<bool> IsDelete { get; set; }

        public virtual ICollection<Vote> Votes { get; set; }       
    }
}
