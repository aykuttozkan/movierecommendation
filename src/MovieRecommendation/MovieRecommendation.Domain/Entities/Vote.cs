﻿using MovieRecommendation.Domain.Abstract;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieRecommendation.Domain.Entities
{
    public class Vote : BaseEntity<int>
    {
        /// <summary>
        /// Kullanici Id
        /// </summary>
        //[ForeignKey("UserId")]
        public int UserId { get; set; }

        /// <summary>
        /// Film Id
        /// </summary>
        public int MovieId { get; set; }

        /// <summary>
        /// Kullanici Puani
        /// </summary>
        public byte VoteValue { get; set; }

        /// <summary>
        /// Kullanici Yorumu
        /// </summary>
        public string VoteNote { get; set; }
    }
}
