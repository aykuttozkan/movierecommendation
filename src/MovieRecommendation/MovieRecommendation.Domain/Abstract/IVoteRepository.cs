﻿using MovieRecommendation.Domain.Dtos;
using MovieRecommendation.Domain.Entities;
using System.Threading.Tasks;

namespace MovieRecommendation.Domain.Abstract
{
    public interface IVoteRepository : IRepository<Vote>
    {
        Task AddCommentAndVote(CommentAndVoteDto commentAndVote, int userID);
    }
}
