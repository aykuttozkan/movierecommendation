﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRecommendation.Domain.Abstract
{
    public interface IMailTools
    {
        Task<bool> SendEmailAsync(string emailAddress, string subject, string message);
    }
}
