﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MovieRecommendation.Domain.Abstract
{
    /// <summary>
    /// Generic repository sinifi olup EntityDal siniflari bu siniftan miras alinmaktadir.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity> where TEntity : class
    {
        ValueTask<TEntity> GetByIdAsync(int id);
        
        Task<IEnumerable<TEntity>> GetAllAsync();
        
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);

        IQueryable<TEntity> FindQueryable(Expression<Func<TEntity, bool>> predicate);
        
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);
        
        Task AddAsync(TEntity entity);
        
        Task AddRangeAsync(IEnumerable<TEntity> entities);
        
        void Remove(TEntity entity);
        
        void RemoveRange(IEnumerable<TEntity> entities);
    }
}
