﻿namespace MovieRecommendation.Domain.Abstract
{
    public interface IConfMailServer
    {
       string Host { get; set; }

       int SmtpPort { get; set; }

       bool EnableSSL { get; set; }

       string CredentialUser { get; set; }

       string CredentialPass { get; set; }
    }
}
