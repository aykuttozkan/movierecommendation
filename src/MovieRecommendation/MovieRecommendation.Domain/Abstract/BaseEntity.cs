﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieRecommendation.Domain.Abstract
{
    /// <summary>
    /// Tum Entity siniflarinda ortak olan bilgilerin bir yerden kontrol edilmesi icin
    /// olusturulan BaseEntity sinifidir.
    /// </summary>
    /// <typeparam name="TType"></typeparam>
    public abstract class BaseEntity<TType>
    {
        /// <summary>
        /// Her tabloda olmasi gereken id bilgisi bulunmakta olup,
        /// Id bilgisine ait key ve identity gibi bilgilerin tanimlamasi yapilmistir.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public TType Id { get; set; }
    }
}
