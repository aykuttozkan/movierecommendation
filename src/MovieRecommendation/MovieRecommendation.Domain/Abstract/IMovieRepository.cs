﻿using MovieRecommendation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRecommendation.Domain.Abstract
{
    public interface IMovieRepository : IRepository<Movie>
    {
        /// <summary>
        /// Tum film bilgilerinin geriye dondurulmesini saglar
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Movie>> GetAllWithMovieAsync();
        
        /// <summary>
        /// Id bilgisine ile eslesen film bilgisinin geri dondurulmesini saglar
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Movie> GetWithMovieByIdAsync(int id);

        /// <summary>
        /// SyncId bilgisi ile eslesen film bilgisinin dondurulmesini saglar
        /// </summary>
        /// <param name="syncId"></param>
        /// <returns></returns>
        Task<Movie> GetWithMovieBySyncIdAsync(int syncId);

        Task<IEnumerable<Movie>> GetAllWithMovieByMovieIdAsync(int movieId);
    }
}
