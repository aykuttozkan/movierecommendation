﻿using MovieRecommendation.Domain.Dtos;
using MovieRecommendation.Domain.Entities;
using System.Threading.Tasks;

namespace MovieRecommendation.Domain.Abstract
{
    /// <summary>
    /// Kullanici ile ilgili islemlerin yapilmasi icin dal layer seviyesinde kullanilan interfacedir
    /// </summary>
    public interface IUserRepository : IRepository<User>
    {
        /// <summary>
        /// Dal seviyesinde kullanici adi ve sifre bilgisinin kontrol edildigi arayuz methodurur.
        /// </summary>
        /// <param name="loginDto"></param>
        /// <returns></returns>
        Task<User> LoginAsync(LoginDto loginDto);
    }
}
