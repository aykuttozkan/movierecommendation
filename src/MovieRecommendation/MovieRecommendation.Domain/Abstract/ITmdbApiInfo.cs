﻿namespace MovieRecommendation.Domain.Abstract
{
    /// <summary>
    /// AppSettings.json sinifindan verilerin okunup dinamik bir sekilde cast edilmesi icin kullanilan interface nesnesidir.
    /// </summary>
    public interface ITmdbApiInfo
    {
        public string ApiUrl { get; set; }

        /// <summary>
        /// Api 3 yapisi ile veri almak icin kullanilan token bilgisi
        /// <code>request islemi icin: ApiUrl?api_key=V3Auth formatinda sorgulama yapilmaktadir.</code>
        /// </summary>
        public string V3Auth { get; set; }

        /// <summary>
        /// Api 4 yapisi ile veri almak icin kullanilan token bilgisi
        /// <code>request islemi icin request header icerisine Authorization: Bearer V4Auth token bilgisini kullanmamiz gerekmektedir. </code>
        /// </summary>
        public string V4Auth { get; set; }

        /// <summary>
        /// Servisten donen result icerisinde bulunan backdrop_path veya poster_path alani icerisindeki bilgi ile ilgili resim verisini almak icin kullanilacak
        /// resim url bilgisi
        /// <code>Ornegin: https://image.tmdb.org/t/p/w500/pcDc2WJAYGJTTvRSEIpRZwM3Ola.jpg</code>
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// Servis uzerinden film bilgilerini almak icin kullanilacak url bilgisi
        /// <code>Ornegin https://api.themoviedb.org/3/discover/movie?api_key=asd&language=tr-TR&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_watch_monetization_types=flatrate </code>
        /// </summary>
        public string Discover { get; set; }
    }
}
