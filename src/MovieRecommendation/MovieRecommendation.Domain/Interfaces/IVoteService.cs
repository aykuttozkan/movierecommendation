﻿using MovieRecommendation.Domain.Dtos;
using System.Threading.Tasks;

namespace MovieRecommendation.Domain.Interfaces
{
    public interface IVoteService
    {
        Task<ResultModel> AddCommentAndRate(CommentAndVoteDto commentAndVote, int userID);
    }
}
