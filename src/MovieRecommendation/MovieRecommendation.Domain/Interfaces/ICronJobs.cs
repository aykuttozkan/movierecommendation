﻿using MovieRecommendation.Domain.Dtos;
using System.Threading.Tasks;

namespace MovieRecommendation.Domain.Interfaces
{
    public interface ICronJobs
    {
        /// <summary>
        /// Sync isleminin baslatilmasini sagliyor
        /// </summary>
        /// <returns></returns>
        Task RunSyncTmdbMovieAsync();

        /// <summary>
        /// Mevcutta calisan sync bilgilerini dondurur
        /// </summary>
        /// <returns></returns>
        Task<ResultModel> GetSyncStatusAsync();
    }
}
