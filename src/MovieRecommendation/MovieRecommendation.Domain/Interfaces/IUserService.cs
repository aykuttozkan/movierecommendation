﻿using MovieRecommendation.Domain.Dtos;
using MovieRecommendation.Domain.Entities;
using System.Threading.Tasks;

namespace MovieRecommendation.Domain.Interfaces
{
    public interface IUserService
    {
        /// <summary>
        /// <seealso cref="LoginDto"/> nesnesi ile gonderilen bilgilere gore
        /// veritabani uzerinde kontrol yapip bilgiler dogru ise geriye
        /// <seealso cref="User"/> nesnesini dondurur
        /// </summary>
        /// <param name="loginDto"></param>
        /// <returns></returns>
        Task<User> LoginAsync(LoginDto loginDto);

        /// <summary>
        /// UserId bilgisi ile eslesen veritabani kullanicisina
        /// ait bilgilerin geri alinmasini saglar
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<User> GetUserAsync(int userId);
    }
}
