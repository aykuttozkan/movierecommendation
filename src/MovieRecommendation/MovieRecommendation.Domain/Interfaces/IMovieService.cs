﻿using MovieRecommendation.Domain.Dtos;
using MovieRecommendation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MovieRecommendation.Domain.Interfaces
{
    public interface IMovieService
    {
        /// <summary>
        /// Tum film bilgilerini veritabanindan almak icin kullaniliyor
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Movie>> GetAllMovies();

        /// <summary>
        /// <seealso cref="PaginationDto"/> bilgilerine gore filtreleme yapip geriye veri dondurulmesini saglar
        /// </summary>
        /// <param name="paginationDto"></param>
        /// <returns></returns>
        Task<ResultMovieWithPaginationModel> GetAllMoviesByPageFilter(PaginationDto paginationDto);

        /// <summary>
        /// Id bilgisine gore veritabanindan ilgili filmi getirmek icin kullanilir
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResultMovieDto> GetMovieById(int id, int userId);
    
        /// <summary>
        /// Film syncId bilgisine gore veritabani uzerindeki bilgiyi dondurur
        /// </summary>
        /// <param name="syncId"></param>
        /// <returns></returns>
        Task<ResultMovieDto> GetMovieBySyncId(int syncId);
    
        /// <summary>
        /// Yeni bir film eklemek icin kullanilir
        /// </summary>
        /// <param name="newMovie"></param>
        /// <returns></returns>
        Task<Movie> CreateMovie(Movie newMovie);
        
        /// <summary>
        /// Mevcutta ki filmi guncellemek icin kullaniliyor
        /// </summary>
        /// <param name="movieToBeUpdated"></param>
        /// <param name="movie"></param>
        /// <returns></returns>
        Task UpdateMovie(Movie movieToBeUpdated, Movie movie);
        
        /// <summary>
        /// Film silmek icin kullaniliyor
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        Task DeleteMovie(Movie movie);

        /// <summary>
        /// Film Tavsiyesi
        /// </summary>
        /// <param name="movieID"></param>
        /// <param name="userID"></param>
        /// <param name="toMail"></param>
        /// <returns></returns>
        Task<bool> Recommend(int movieID, int userID, string toMail);
    }
}
