﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MovieRecommendation.Domain.Dtos
{
    public class CommentAndVoteDto
    {
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "MovieId is min. value must be 1")]
        public int MovieId { get; set; }

        [Required]
        [Range(1,10, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public byte Rating { get; set; }

        [Required]
        [MinLength(5, ErrorMessage = "Min. comment lenght must be 5")]
        [MaxLength(500, ErrorMessage = "Max. comment lenght must be 500")]
        public string Comment { get; set; }
    }
}
