﻿namespace MovieRecommendation.Domain.Dtos
{
    public class UserResponseDto
    {
        public int UserId { get; set; }

        public string Guid { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Token { get; set; }
    }
}
