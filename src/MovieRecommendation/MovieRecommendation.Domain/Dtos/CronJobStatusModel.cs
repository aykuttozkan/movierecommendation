﻿using System;

namespace MovieRecommendation.Domain.Dtos
{
    public class CronJobStatusModel
    {
        public int Step { get; set; }

        public Guid ProcessId { get; set; }

        public string ProcessDetails { get; set; }

        public int TotalProcess { get; set; }
    }
}
