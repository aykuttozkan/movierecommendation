﻿namespace MovieRecommendation.Domain.Dtos
{
    public class ResultModel
    {
        /// <summary>
        /// Islem sirasinda bir hata meydana geldiyse True degeri atanmalidir.
        /// Boylelikle geri bildirimler yapilmadan once islem sirasinda bir hata olup olmadigi kontrol edilebilir
        /// </summary>
        public bool Error { get; set; }

        /// <summary>
        /// Islem sirasinda karsilasilan hatalara ait mesajlarinin atandigi property
        /// </summary>
        public string ErrorResult { get; set; }

        /// <summary>
        /// Geri dondurulmesini istedigimiz iceriklerin atandigi property
        /// </summary>
        public object Result { get; set; }

        /// <summary>
        /// Istege ait geri donus degeri
        /// </summary>
        public int StatusCode { get; set; }
    }
}
