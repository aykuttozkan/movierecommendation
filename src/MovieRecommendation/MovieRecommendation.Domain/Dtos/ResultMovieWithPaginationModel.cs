﻿using System.Collections.Generic;

namespace MovieRecommendation.Domain.Dtos
{
    public class ResultMovieWithPaginationModel
    {
        public IEnumerable<ResultMovieDto> Movies { get; set; }
        
        public int Page { get; set; }
        
        public int TotalPage { get; set; }

        public int TotalItem { get; set; }
    }
}
