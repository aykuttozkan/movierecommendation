﻿using MovieRecommendation.Domain.Abstract;

namespace MovieRecommendation.Domain.Dtos
{
    public class ConfMailServer : IConfMailServer
    {
        public string Host { get; set; }
        
        public int SmtpPort { get; set; }
        
        public bool EnableSSL { get; set; }
        
        public string CredentialUser { get; set; }
        
        public string CredentialPass { get; set; }
    }
}
