﻿namespace MovieRecommendation.Domain.Dtos
{
    public class OperationClaim
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
