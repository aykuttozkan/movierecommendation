﻿namespace MovieRecommendation.Domain.Dtos
{
    public class JwtConfigModel
    {
        public string Secret { get; set; }

        public string Issuer { get; set; }

        public string Audience { get; set; }

        public int ExpMinute { get; set; }
    }
}
