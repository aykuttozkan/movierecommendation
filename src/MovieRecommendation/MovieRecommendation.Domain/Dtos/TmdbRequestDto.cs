﻿using System;
using System.Collections.Generic;

namespace MovieRecommendation.Domain.Dtos
{
    public class TmdbRequestDto
    {
        /**
            page : 1
            results : [
                    "adult": false,
                    "backdrop_path": "/inJjDhCjfhh3RtrJWBmmDqeuSYC.jpg",
                    "genre_ids": [
                        28,
                        878
                    ],
                    "id": 399566,
                    "original_language": "en",
                    "original_title": "Godzilla vs. Kong",
                    "overview": "Devasa Kong, durdurulamayan Godzilla ile tanışır. Dünya, hangisinin tüm canavarların kralı olduğunu görmek için izler.",
                    "popularity": 5581.344,
                    "poster_path": "/pgqgaUx1cJb5oZQQ5v0tNARCeBp.jpg",
                    "release_date": "2021-03-24",
                    "title": "Godzilla vs. Kong",
                    "video": false,
                    "vote_average": 8.3,
                    "vote_count": 4450
                ],
            "total_pages": 500,
            "total_results": 10000
         */

        public int page { get; set; }

        public int total_pages { get; set; }

        public int total_results { get; set; }

        public List<TmDbResult> results { get; set; }
        
        //public string results { get; set; }

    }

    public class TmDbResult
    {
        public int id { get; set; }
        
        public string original_language { get; set; }
        
        public string original_title { get; set; }
        
        public string overview { get; set; }
        
        public float popularity { get; set; }
        
        public string poster_path { get; set; }
        
        public string release_date { get; set; }
        
        public string title { get; set; }
        
        public bool video { get; set; }
        
        public float vote_average { get; set; }
        
        public int vote_count { get; set; }
        
        public string backdrop_path { get; set; }
        
        public bool adult { get; set; }
    }
}
