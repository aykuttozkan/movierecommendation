﻿using MovieRecommendation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRecommendation.Domain.Dtos
{
    public class ResultMovieDto
    {
        public int Id { get; set; }
        
        public int SyncId { get; set; }

        public string Title { get; set; }

        public string Overview { get; set; }

        public string OriginalLanguage { get; set; }

        public string OriginalTitle { get; set; }

        public Nullable<float> Popularity { get; set; }

        public string PosterPath { get; set; }

        public string ReleaseDate { get; set; }

        public Nullable<bool> Video { get; set; }

        public string BackdropPath { get; set; }

        public double VoteAverage { get; set; }

        public int VoteCount { get; set; }

        public byte VoteValue { get; set; }

        public string VoteNote { get; set; }

        public Vote Vote { get; set; }
        
        
    }
}
