﻿namespace MovieRecommendation.Domain.Dtos
{
    public class PaginationDto
    {
        private string _OrderBy;

        /// <summary>
        /// Gidilmek istenen sayfa bilgisi
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// Sayfa basina gosterilecek veri sayisi
        /// </summary>
        public int ItemPerPage { get; set; }

        public string OrderBy { 
            get { 
                return _OrderBy; 
            } 
            set { 
                if(string.IsNullOrWhiteSpace(value))
                {
                    _OrderBy = "asc";
                }
                else
                {
                    _OrderBy = (value != "asc" || value != "desc") ? "asc" : value;
                }
            } 
        }
    }
}
