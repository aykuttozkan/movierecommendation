## Migration işlemi sırasında default verileri eklemek için aşağıdaki komutları;

protected override void Up(MigrationBuilder migrationBuilder) methodu içerisine eklenmelidir.

    migrationBuilder.InsertData("Users"
                , new string[] { "Guid", "Name", "Surname", "UserName", "Email", "Password" }
                , new string[] { Guid.NewGuid().ToString(), "Demo User Name", "Surname", "demouser", "demo@demo.com", "123456".ToSha512() });

## Veritabanını Oluşturmak

Veritabanını oluşturmak icin visual studio üzerinde package manager console penceresini aktif hale getirip;
    
    update-database komutunu çalıştırıyoruz

Eğer migration klasörü bulunmuyorsa package manager console penceresine aşağıdaki komutları giriniz
    
    add-migration Init
    update-database


## Swagger Kullanımı

1. Projeyi visual studio ile çalıştırdıktan sonra default olarak swagger ekranı açılacaktır.
2. Açılan ekran üzerinde bazı restapi alanları oturum doğrulama işlemi ile birlikte bazıları ise doğrudan çalışmaktadır ve yetki ile erişim gereken servislerin yan tarafında anahtar işareti belirmektedir.
3. Oturum doğrulama işlemi için **/api/Account/login** seçimi yapıdıktan sonra **try it** menüsü seçilir ve çıkan alana aşağıdaki bilgiler eklenir.

        {
            "userName": "demouser",
            "password": "123456"
        }

4. Yukarı da default kullanıcı bilgileri olup bu bilgileri ilgili alana yazdıktan sonra **Execute** menüsüne tıklamanız gerekmektedir.
5. İşlemin başarılı olması durumunda yanıt olarak dönen **token** bilgisini kopyalayınız.
6. Elde etmiş olduğunuz token bilgisini sisteme tanıtabilmek için; sayfanın sağ üst tarafında bulunan **Authorize** butonuna tıklayıp, açılan ekran da aşağıdaki formatta token bilgisini giriniz. *(Kopyalamış olduğunuz token bilgisini **tokenbilgisi** yazan alana giriniz.)*

        "Bearer tokenBilgisi"

7. Bu aşamadan sonra anahtar işareti bulunan alanlar üzerinde sorgulama yapabiliyor olmanız gerekmektedir.


> NOT! Jwt token süresi appsettings.json dosyası içerisinde **"ExpMinute": 5** olarak tanımlanmış olup, bu süreyi istediğiniz şekilde değiştirerek, süreyi uzatıp, kısaltabilirsiniz.


## Hangire Ayarları

 Proje üzerinde zamanlanmış görevler için **hangfire** kütüphanesi kullanılmakta olup, default olarak **59dk** aralıklarla çalışacak şekilde tanımlama yapılmıştır. Bu süreyi ayarlamak için;
 
 1. **MovieRecommendation.WebApi/Hangire/HangfireJobs.cs** sınıfına gidiniz.
 2. Bu sınıf içerisinde bulunana **Init** methodu altında ki;


        RecurringJob.AddOrUpdate("JobId", () => CronJobs.RunSyncTmdbMovieAsync(), "*/59 * * * *", queue: "default");

        // **59** ile başlayan yeri değiştiriniz.


3. Gerekli değişiklikleri ve tanımlamaları yaptıktan sonra projeyi çalıştırınız

4. Hangfire arayüz ekranına erişim sağlayabilmek için;

        http(s)://localhost:(portNo)/hangfire

    adresine gidiniz.
5. Bu ekren üzerinden çalışan ve çalışmış olan tüm zamanlanmış görevleri anlık olarak kontrol edebilirsiniz.


## Smtp Server'ın çalıştırılması

1. Proje üzerinde mail gönderim testlerinin yapılabilmesi için kullanılabilecek, smtp server için docker **smtp4dev** projesinden yararlanılmıştır. Bundan dolayı proje üzerinde deneme amaçlı mail gönderimleri için aşağıdaki komutu komut satırında çalıştırarak bir adet smtp server oluşturabilirsiniz. Smtp server tamamen demo amaçlı olup herhangi bir yere mail gönderimi yapılmamakta fakat gönderimin simüle edilebilmesi sağlanmaktadır.


        docker run --rm -it -p 3000:80 -p 25:25 rnwood/smtp4dev:v3


2. Yukarıdaki komutu çalıştırdıktan sonra **25** nolu port aktif hale getirilecek olup, **3000** nolu port üzerinde ise anlık olarak gönderilen maillerin takibi yapılabilecektir.
3. Canlı ortama geçiş için **appsettings.json** dosyası üzerinde düzeneleme yapmanız yeterli olacaktır.

