## Film tavsiye uygulamasına ait bilgi dokümanı

* Proje kaynak kodları **src/** dizini altında olup, projeyi indirdikten sonra çalıştırmak için gerekli olan adımlar **src/Readme.md** dosyası içerisinde mevcuttur.

* Projeye ait detaylı dokümanlar (sınıflar, methodlar, propery'ler) olmak üzere tüm bilgilere ise **doc/html/** klasörü altında ki **index.html** sayfasını açarak erişim sağlayabilirsiniz.

* Projenin çalışmış haline ait **gif** formatındaki ekran görüntüleri ise **screenshots/** klasörü altında erişilebilir durumdadır.

* Proje de veritabanı olarak **sqlite** kullanılmakta olup, herhangi bir kuruluma gerek olmamaktadır.

* Zamanlanmış görevler için yazılan sınıflara ek olarak **hangfire** kütüphanesinden faydalanılmış olup, projeyi çalıştırdıktan sonra; **localhost:{portNo}/hangfire** adresi ile erişim sağlayabilir, aktif olan ve çalışan tüm task'leri kontrol edebilirsiniz.